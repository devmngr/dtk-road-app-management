﻿using Adapter;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ModelManager
{
    public class ModelManagerImpl : IModelManager
    {
        private IDatabaseAdapter adapter;

        public ModelManagerImpl()
        {
            adapter = new DatabaseAdapterImpl();
        }

        public int AddGasStation(GasStation gasStation)
        {
            return adapter.AddGasStation(gasStation);
        }
        public int AddTruckWash(TruckWash truckWash)
        {
            return adapter.AddTruckWash(truckWash);
        }
        public int AddOffice(Office office)
        {
            return adapter.AddOffice(office);
        }
        public int AddEmployee(Employee employee)
        {
            return adapter.AddEmployee(employee);
        }
        public int AddLoginUser(LoginUser loginUser)
        {
            return adapter.AddLoginUser(loginUser);
        }
        public int AddBroker(Broker broker)
        {
            return adapter.AddBroker(broker);
        }
        public int AddHaulier(Haulier haulier)
        {
            return adapter.AddHaulier(haulier);
        }



        public int UpdateGasStation(GasStation gasStation)
        {
            return adapter.UpdateGasStation(gasStation);
        }
        public int UpdateTruckWash(TruckWash truckwash)
        {
            return adapter.UpdateTruckWash(truckwash);
        }
        public int UpdateOffice(Office office)
        {
            return adapter.UpdateOffice(office);
        }
        public int UpdateEmployee(Employee employee)
        {
            return adapter.UpdateEmployee(employee);
        }
        public int UpdateLoginUser(LoginUser loginUser)
        {
            return adapter.UpdateLoginUser(loginUser);
        }
        public int UpdateBroker(Broker broker)
        {
            return adapter.UpdateBroker(broker);
        }
        public int UpdateHaulier(Haulier haulier)
        {
            return adapter.UpdateHaulier(haulier);
        }




        public bool TruckWashExists(TruckWash truckWash)
        {
            return adapter.TruckWashExists(truckWash);
        }

        public bool OfficeExists(Office office)
        {
            return adapter.OfficeExists(office);
        }

        public bool EmployeeExists(Employee employee)
        {
            return adapter.EmployeeExists(employee);
        }

        public bool LoginUserExists(LoginUser loginUser)
        {
            return adapter.LoginUserExists(loginUser);
        }

        public bool BrokerExists(Broker broker)
        {
            return adapter.BrokerExists(broker);
        }

        public bool HaulierExists(Haulier haulier)
        {
            return adapter.HaulierExists(haulier);
        }





        public int DeleteAllFromGasStation()
        {
            return adapter.DeleteAllFromTable(Table.GasStation);
        }
        public int DeleteAllFromTruckWash()
        {
            return adapter.DeleteAllFromTable(Table.TruckWash);
        }

        public int DeleteGasStation(GasStation gasStation)
        {
            return adapter.DeleteFromTableById(Table.GasStation, gasStation.Id);
        }
        public int DeleteTruckWash(TruckWash truckwash)
        {
            return adapter.DeleteFromTableById(Table.TruckWash, truckwash.Id);
        }
        public int DeleteOffice(Office office)
        {
            return adapter.DeleteFromTableById(Table.Office, office.Id);
        }
        public int DeleteEmployee(Employee employee)
        {
            return adapter.DeleteFromTableById(Table.Employee, employee.Id);
        }
        public int DeleteLoginUser(LoginUser loginUser)
        {
            return adapter.DeleteFromTableById(Table.LoginUser, loginUser.Id);
        }
        public int DeleteBroker(Broker broker)
        {
            return adapter.DeleteFromTableById(Table.Broker, broker.Id);
        }
        public int DeleteHaulier(Haulier haulier)
        {
            return adapter.DeleteFromTableById(Table.Haulier, haulier.Id);
        }
        public int DeleteAllHaulierOfBroker(Broker broker)
        {
            return adapter.DeleteFromTableByColumn(Table.Haulier, "brokerid", broker.Id);
        }
        public DataTable AllTruckWash()
        {
            return adapter.AllTruckWash();
        }
        public DataTable AllGasStation()
        {
            return adapter.AllGasStation();
        }
        public DataTable AllOffice()
        {
            return adapter.AllOffice();
        }
        public DataTable AllEmployee()
        {
            return adapter.AllEmployee();
        }
        public DataTable AllLoginUser()
        {
            return adapter.AllLoginUser();
        }
        public DataTable AllBroker()
        {
            return adapter.AllBroker();
        }
        public DataTable AllHaulier()
        {
            return adapter.AllHaulier();
        }








        /*
                public List<GasStation> AllGasStation()
                {
                    List<GasStation> list = new List<GasStation>();
                    DataTable dataTable = adapter.AllGasStation();

                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        int id = Convert.ToInt32(dataTable.Rows[i]["Id"]);
                        string type = dataTable.Rows[i]["Type"].ToString();
                        string name = dataTable.Rows[i]["Name"].ToString();
                        string address = dataTable.Rows[i]["Address"].ToString();
                        string zip = dataTable.Rows[i]["Zip"].ToString();
                        string city = dataTable.Rows[i]["City"].ToString();
                        string longitude = dataTable.Rows[i]["Longitude"].ToString();
                        string latitude = dataTable.Rows[i]["Latitude"].ToString();
                        int adblue = Convert.ToInt32(dataTable.Rows[i]["adblue"]);
                        GasStation gasStation =
                            new GasStation(id, type, name, address, zip,
                            city, longitude, latitude, adblue);
                        list.Add(gasStation);
                    }



                    return list;
                }
                public List<TruckWash> AllTruckWash()
                {
                    List<TruckWash> list = new List<TruckWash>();
                    DataTable dataTable = adapter.AllTruckWash();

                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        int id = Convert.ToInt32(dataTable.Rows[i]["Id"]);
                        string name = dataTable.Rows[i]["Name"].ToString();
                        string address = dataTable.Rows[i]["Address"].ToString();
                        string zip = dataTable.Rows[i]["Zip"].ToString();
                        string city = dataTable.Rows[i]["City"].ToString();
                        string phone = dataTable.Rows[i]["Phone"].ToString();
                        string email = dataTable.Rows[i]["Email"].ToString();
                        string openingHours = dataTable.Rows[i]["OpeningHours"].ToString();

                        string truckStandardPrice = dataTable.Rows[i]["TruckStandardPrice"].ToString();
                        string trailerStandardPrice = dataTable.Rows[i]["TrailerStandardPrice"].ToString();
                        string truckTrailerStandardPrice = dataTable.Rows[i]["TruckTrailerStandardPrice"].ToString();
                        string truckQuickWashPrice = dataTable.Rows[i]["TruckQuickWashPrice"].ToString();
                        string trailerQuickWashPrice = dataTable.Rows[i]["TrailerQuickWashPrice"].ToString();
                        string truckTrailerQuickWashPrice = dataTable.Rows[i]["TruckTrailerQuickWashPrice"].ToString();
                        string remarks = dataTable.Rows[i]["Remarks"].ToString();
                        string website = dataTable.Rows[i]["Website"].ToString();
                        string longitude = dataTable.Rows[i]["Longitude"].ToString();
                        string latitude = dataTable.Rows[i]["Latitude"].ToString();

                        TruckWash truckWash =
                            new TruckWash(id, name, address, zip,
                            city, phone, email, openingHours,
                            truckStandardPrice, trailerStandardPrice, truckTrailerStandardPrice,
                            truckQuickWashPrice, trailerQuickWashPrice, truckTrailerQuickWashPrice,
                            remarks, website, longitude, latitude);
                        list.Add(truckWash);
                    }
                    return list;
                }

            */



    }
}
