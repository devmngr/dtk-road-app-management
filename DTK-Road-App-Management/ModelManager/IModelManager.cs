﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adapter;
using Model;
namespace ModelManager
{
    public interface IModelManager
    {

        int AddTruckWash(TruckWash truckWash);
        int AddGasStation(GasStation gasStation);
        int AddOffice(Office office);
        int AddEmployee(Employee employee);
        int AddLoginUser(LoginUser loginUser);
        int AddBroker(Broker broker);
        int AddHaulier(Haulier haulier);



        int UpdateTruckWash(TruckWash truckwash);
        int UpdateGasStation(GasStation gasStation);
        int UpdateOffice(Office office);
        int UpdateEmployee(Employee employee);
        int UpdateLoginUser(LoginUser loginUser);
        int UpdateBroker(Broker broker);
        int UpdateHaulier(Haulier haulier);


        bool TruckWashExists(TruckWash truckWash);
        bool OfficeExists(Office office);
        bool EmployeeExists(Employee employee);
        bool LoginUserExists(LoginUser loginUser);
        bool BrokerExists(Broker broker);
        bool HaulierExists(Haulier haulier);


        int DeleteAllFromTruckWash();
        int DeleteAllFromGasStation();

        int DeleteTruckWash(TruckWash truckwash);
        int DeleteGasStation(GasStation gasStation);
        int DeleteOffice(Office office);
        int DeleteEmployee(Employee employee);
        int DeleteLoginUser(LoginUser loginUser);
        int DeleteHaulier(Haulier haulier);
        int DeleteBroker(Broker broker);
        int DeleteAllHaulierOfBroker(Broker broker);


        DataTable AllTruckWash();
        DataTable AllGasStation();
        DataTable AllOffice();
        DataTable AllEmployee();
        DataTable AllLoginUser();
        DataTable AllBroker();
        DataTable AllHaulier();



        /*
        List<TruckWash> AllTruckWash();
        List<GasStation> AllGasStation();
        */

    }
}
