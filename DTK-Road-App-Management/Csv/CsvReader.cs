﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csv
{
    public class CsvReader
    {

        private char delimiter;
        private List<string> csvFile;



        public CsvReader(char delimiter)
        {
            this.delimiter = delimiter;
            csvFile = new List<string>();
        }
        public char Delimiter { get => delimiter; set => delimiter = value; }
        public List<string> readCsvFile(string filePath)
        {
            var reader = new StreamReader(File.OpenRead(filePath), System.Text.Encoding.GetEncoding("iso-8859-1"));
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                line = line.Remove(line.Length - 1);
                csvFile.Add(line);
            }
            reader.Close();
            return csvFile;
        }

        public string[] readLineValues(int index)
        {
            string[] values = csvFile[index].Split(delimiter).Select(sValue => sValue.Trim()).ToArray();
            return values;
        }

        public int numberOfLines()
        {
            return csvFile.Count();
        }
        public int numberOfValuesInLine(int index)
        {
            string[] values = readLineValues(index);
            return values.Length;
        }
        public DataTable CsvToDataTable(string filePath, string[] columnNames)
        {

            DataTable dt = new DataTable();
            DataRow row = null;
            readCsvFile(filePath);
            string[] arr = null;

            for (int i = 0; i < numberOfLines(); i++)
            {
                arr = readLineValues(i);
                if (i == 0)
                {
                    AddColumns(dt, columnNames);
                }
                else
                {
                    row = dt.NewRow();
                    AddToRow(row, arr);
                    dt.Rows.Add(row);
                }
            }
            return dt;
        }

        private void AddToRow(DataRow row, string[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
                row[i] = arr[i];
        }

        private void AddColumns(DataTable dt, string[] arr)
        {
            foreach (string str in arr)
                dt.Columns.Add(str, typeof(string));
        }


        /*  private void AddColumns(DataTable dt, string[] columns)
          {
              foreach (string str in columns)
              {
                  dt.Columns.Add(str);
              }
          }*/


        /*
                private static DataTable GetDataTabletFromCSVFile(string csv_file_path)

                {
                    DataTable csvData = new DataTable();
                    try

                    {
                        using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                        {
                            csvReader.SetDelimiters(new string[] { "," });
                            csvReader.HasFieldsEnclosedInQuotes = true;
                            string[] colFields = csvReader.ReadFields();

                            foreach (string column in colFields)
                            {
                                DataColumn datecolumn = new DataColumn(column);
                                datecolumn.AllowDBNull = true;
                                csvData.Columns.Add(datecolumn);
                            }
                            while (!csvReader.EndOfData)
                            {
                                string[] fieldData = csvReader.ReadFields();
                                //Making empty value as null
                                for (int i = 0; i < fieldData.Length; i++)
                                {
                                    if (fieldData[i] == "")

                                    {

                                        fieldData[i] = null;

                                    }
                                }
                                csvData.Rows.Add(fieldData);
                            }
                        }
                    }

                    catch (Exception ex)
                    {

                    }
                    return csvData;
                }
                */









    }
}
