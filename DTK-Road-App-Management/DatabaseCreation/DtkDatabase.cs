﻿using Adaptee;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseCreation
{
    public class DtkDatabase
    {
        private string connectionString;
        private SQLiteConnection connection;
        private string databaseName, path;

        private static string EXTENSION = ".db";
        public DtkDatabase(string path, string databaseName)
        {
            this.path = path;
            this.databaseName = databaseName;
            CreateDatabaseFile();
            CreateGasStationTable();
            CreateTruckWashTable();
            CreateOfficeTable();
            CreateLoginUserTable();
            CreateEmployeeTable();
            CreateBrokerTable();
            CreateHaulierTable();
        }

        private void CreateDatabaseFile()
        {
            string temp = path + databaseName + EXTENSION;
            SQLiteConnection.CreateFile(temp);
            connectionString = "Data Source=" + temp + ";Version=3;";
        }
        private void CreateGasStationTable()
        {

            string sql =
                "CREATE TABLE [Gas_Station] " +
                "(" +
                    "[Id] INTEGER NOT NULL, " +
                    "[Type] TEXT , " +
                    "[Name] TEXT ," +
                    "[Address] TEXT , " +
                    "[Zip] TEXT , " +
                    "[City] TEXT , " +
                    "[Longitude] TEXT , " +
                    "[Latitude] TEXT , " +
                    "[AdBlue] TEXT , " +
                    "CONSTRAINT[PK_gas_station_table] PRIMARY KEY([Id])" +
                ");";
            Execute(sql);

        }
        private void CreateTruckWashTable()
        {


            string sql =
                "CREATE TABLE [Truck_Wash] " +
                "(" +
                    "[Id] INTEGER NOT NULL, " +
                    "[Name] TEXT , " +
                    "[Address] TEXT , " +
                    "[Zip] TEXT , " +
                    "[City] TEXT , " +
                    "[Phone] TEXT , " +
                    "[Email] TEXT , " +
                    "[OpeningHours] TEXT , " +
                    "[TruckStandardPrice] TEXT , " +
                    "[TrailerStandardPrice] TEXT , " +
                    "[TruckTrailerStandardPrice] TEXT , " +
                    "[TruckQuickWashPrice] TEXT , " +
                    "[TrailerQuickWashPrice] TEXT , " +
                    "[TruckTrailerQuickWashPrice] TEXT , " +
                    "[Remarks] TEXT , " +
                    "[Website] TEXT , " +
                    "[Latitude] TEXT , " +
                    "[Longitude] TEXT , " +
                    "CONSTRAINT[PK_truck_wash_table] PRIMARY KEY([Id])" +
                ");";

            Execute(sql);

        }
        private void CreateOfficeTable()
        {
            string sql =
                "CREATE TABLE [Office] (" +
                "[Id] INTEGER NOT NULL" +
                ", [Name] TEXT NOT NULL" +
                ", [Address] TEXT NOT NULL" +
                ", [ZipCode] TEXT NOT NULL" +
                ", [City] TEXT NOT NULL" +
                ", [PhoneNr] TEXT NOT NULL" +
                ", [FaxNr] TEXT NOT NULL" +
                ", [Email] TEXT NOT NULL" +
                ", [Latitude] TEXT NOT NULL" +
                ", [Longitude] TEXT NOT NULL" +
                ", CONSTRAINT[PK_office] PRIMARY KEY([Id])" +
                ");";

            Execute(sql);
        }
        private void CreateLoginUserTable()
        {
            string sql =
                "CREATE TABLE [LoginUser] (" +
                "[Id] INTEGER NOT NULL" +
                ", [Username] TEXT NOT NULL" +
                ", [Password] TEXT NOT NULL" +
                ", [Rights] TEXT NOT NULL" +
                ", CONSTRAINT[PK_LoginUser] PRIMARY KEY([Id])" +
                ");";

            Execute(sql);
        }
        private void CreateEmployeeTable()
        {
            string sql =
                "CREATE TABLE [Employee] (" +
                "[Id] INTEGER NOT NULL" +
                ", [Picture] BLOB " +
                ", [Name] TEXT NOT NULL" +
                ", [Position] TEXT" +
                ", [Email] TEXT" +
                ", [MobileNr] TEXT" +
                ", [DirekteNr] TEXT" +
                ", [OfficeId] INTEGER" +
                ", [LoginUserId] INTEGER" +
                ", CONSTRAINT[PK_Employee] PRIMARY KEY([Id])" +
                ", FOREIGN KEY([OfficeId]) REFERENCES Office([Id])" +
                ", FOREIGN KEY([LoginUserId]) REFERENCES LoginUser([Id])" +
                ");";

            Execute(sql);
        }
        private void CreateBrokerTable()
        {
            string sql =
                "CREATE TABLE [Broker] (" +
                "[Id] INTEGER NOT NULL" +
                ", [Name] TEXT NOT NULL" +
                ", [Email] TEXT NOT NULL" +
                ", [LoginUserId] INTEGER NOT NULL" +
                ", CONSTRAINT[PK_Broker] PRIMARY KEY([Id])" +
                ", FOREIGN KEY([LoginUserId]) REFERENCES LoginUser([Id])" +
                ");";

            Execute(sql);
        }
        private void CreateHaulierTable()
        {
            string sql =
                 "CREATE TABLE [Haulier] (" +
                 "[Id] INTEGER NOT NULL" +
                 ", [Name] TEXT NOT NULL" +
                 ", [Email] TEXT NOT NULL" +
                 ", [TruckNr] TEXT NOT NULL" +
                 ", [BrokerId] INTEGER" +
                 ", [LoginUserId] INTEGER NOT NULL" +
                 ", CONSTRAINT[PK_Broker] PRIMARY KEY([Id])" +
                 ", FOREIGN KEY([BrokerId]) REFERENCES Broker([Id])" +
                 ", FOREIGN KEY([LoginUserId]) REFERENCES LoginUser([Id])" +
                 ");";

            Execute(sql);
        }
        private void Execute(string sql)
        {
            Connect();
            Open();
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            command.ExecuteNonQuery();
            Close();
        }
        private void Open()
        {
            connection.Open();
        }
        private void Connect()
        {
            connection = new SQLiteConnection(connectionString);
        }
        private void Close()
        {
            connection.Close();
        }




    }
}

