﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Reflection;
using Adaptee;
using Model;
using Util;
using static System.Net.Mime.MediaTypeNames;

namespace Adapter
{
    public class DatabaseAdapterImpl : IDatabaseAdapter
    {

        private SqLiteAdaptee adaptee;
        public DatabaseAdapterImpl()
        {
            adaptee = new SqLiteAdaptee();

        }

        public int AddGasStation(GasStation gasStation)
        {

            string sql = "insert into " +
                GetEnumDescription(Table.GasStation) +
                "(type,name,address,zip,city,adblue,longitude,latitude )" +
                "values(@type,@name,@address,@zip,@city,@adblue,@longitude,@latitude)";

            SQLiteCommand command = GetCommandWithGasStationParameters(gasStation, sql);

            return adaptee.Update(command);
        }
        public int AddTruckWash(TruckWash truckWash)
        {
            if (TruckWashExists(truckWash))
                return -1;

            string sql = "insert into " +
                 GetEnumDescription(Table.TruckWash) +
                "(name, address, zip, city, phone, email, openingHours, " +
                "truckStandardPrice, trailerStandardPrice, truckTrailerStandardPrice," +
                "truckQuickWashPrice, trailerQuickWashPrice,truckTrailerQuickWashPrice," +
                "remarks, website,longitude, latitude)" +
                "values(@name, @address, @zip, @city, @phone, @email, @openingHours, " +
                "@truckStandardPrice, @trailerStandardPrice,@truckTrailerStandardPrice," +
                "@truckQuickWashPrice, @trailerQuickWashPrice, @truckTrailerQuickWashPrice," +
                "@remarks, @website, @latitude,@longitude)";

            SQLiteCommand command = GetCommandWithTruckWashParameters(truckWash, sql);

            return adaptee.Update(command);

        }
        public int AddOffice(Office office)
        {
            if (OfficeExists(office))
                return -1;

            string sql = "insert into " +
                GetEnumDescription(Table.Office) +
            "(name, address,zipCode,city,phoneNr,faxNr,email,latitude,longitude)" +
            " values(@name,@address,@zipCode,@city,@phoneNr,@faxNr,@email,@latitude,@longitude)";

            SQLiteCommand command = GetCommandWithOfficeParameters(office, sql);

            return adaptee.Update(command);
        }
        public int AddEmployee(Employee employee)
        {
            if (EmployeeExists(employee))
                return -1;
            else if (employee.HasLoginUser() && LoginUserExists(employee.LoginUser))
                return -2;

            String sql = "";
            if (employee.HasLoginUser())
                sql = "insert into " +
                   GetEnumDescription(Table.Employee) +
               "(picture,name, position,email,mobileNr,direkteNr,officeId,LoginUserId)" +
               "values(@picture,@name, @position,@email,@mobileNr,@direkteNr,@officeId,@LoginUserId)";

            else
                sql = "insert into " +
                   GetEnumDescription(Table.Employee) +
               "(picture,name, position,email,mobileNr,direkteNr,officeId)" +
               "values(@picture,@name, @position,@email,@mobileNr,@direkteNr,@officeId)";

            SQLiteCommand command = GetCommandWithEmployeeParameters(employee, sql);

            return adaptee.Update(command);
        }
        public int AddLoginUser(LoginUser loginUser)
        {
            if (LoginUserExists(loginUser))
                return -1;

            string sql = "insert into " +
                GetEnumDescription(Table.LoginUser) +
            "(username,password,rights)" +
            "values(@username,@password,@rights)";

            SQLiteCommand command = GetCommandWithLoginUserParameters(loginUser, sql);

            return adaptee.Update(command);
        }
        public int AddBroker(Broker broker)
        {
            if (BrokerExists(broker))
                return -1;
            else if (LoginUserExists(broker.LoginUser))
                return -2;

            string sql = "insert into " +
               GetEnumDescription(Table.Broker) +
           "(name,email,loginUserId)" +
           "values(@name,@email,@loginUserId)";

            SQLiteCommand command = GetCommandWithBrokerParameters(broker, sql);

            return adaptee.Update(command);
        }
        public int AddHaulier(Haulier haulier)
        {
            string sql = "";
            if (HaulierExists(haulier))
                return -1;
            else if (LoginUserExists(haulier.LoginUser))
                return -2;
            if (haulier.HasBroker())
                sql = "insert into " +
                  GetEnumDescription(Table.Haulier) +
              "(name,email,truckNr,brokerId,loginUserId)" +
              "values(@name,@email,@truckNr,@brokerId,@loginUserId)";

            else
                sql = "insert into " +
                  GetEnumDescription(Table.Haulier) +
              "(name,email,truckNr,loginUserId)" +
              "values(@name,@email,@truckNr,@loginUserId)";
            SQLiteCommand command = GetCommandWithHaulierParameters(haulier, sql);

            return adaptee.Update(command);
        }

        public int DeleteAllFromTable(Table table)
        {
            string sql = "DELETE FROM " + GetEnumDescription(table);
            SQLiteCommand command = new SQLiteCommand(sql);
            return adaptee.Update(command);
        }
        public int DeleteFromTableById(Table table, int id)
        {
            string sql = "DELETE FROM " + GetEnumDescription(table) + " where id = " + id;
            SQLiteCommand command = new SQLiteCommand(sql);
            return adaptee.Update(command);
        }
        public int DeleteFromTableByColumn(Table table, string column, int id)
        {
            string sql = "DELETE FROM " + GetEnumDescription(table) + " where '" + column + "' = " + id;
            SQLiteCommand command = new SQLiteCommand(sql);
            return adaptee.Update(command);
        }


        public int UpdateGasStation(GasStation gasStation)
        {
            string sql = "Update " +
            GetEnumDescription(Table.GasStation) +
            " SET " +
            "type = @type ," +
            "name = @name ," +
            "address = @address ," +
            "zip = @zip ," +
            "city = @city ," +
            "longitude = @longitude ," +
            "latitude = @latitude ," +
            "adblue = @adblue " +
            " where id = @id";

            SQLiteCommand command = GetCommandWithGasStationParameters(gasStation, sql);
            return adaptee.Update(command);
        }
        public int UpdateTruckWash(TruckWash truckwash)
        {
            string sql = "Update " +
                GetEnumDescription(Table.TruckWash) +
                " SET " +
                "name = @name ," +
                "address = @address ," +
                "zip = @zip ," +
                "city = @city ," +
                "Phone = @phone ," +
                "email = @email ," +
                "openinghours = @openingHours ," +
                "truckStandardPrice = @truckStandardPrice , " +
                "trailerStandardPrice = @trailerStandardPrice ," +
                "truckTrailerStandardPrice = @truckTrailerStandardPrice ," +
                "truckQuickWashPrice = @truckQuickWashPrice ," +
                "trailerQuickWashPrice = @trailerQuickWashPrice ," +
                "truckTrailerQuickWashPrice = @truckTrailerQuickWashPrice ," +
                "remarks = @remarks ," +
                "website = @website ," +
                "latitude = @latitude ," +
                "longitude = @longitude " +
                " where id = @id";

            SQLiteCommand command = GetCommandWithTruckWashParameters(truckwash, sql);
            return adaptee.Update(command);

        }
        public int UpdateOffice(Office office)
        {
            string sql = "Update " +
            GetEnumDescription(Table.Office) +
            " SET " +
            "name = @name ," +
            "address = @address ," +
            "zipCode = @zipCode ," +
            "city = @city ," +
            "phoneNr = @phoneNr ," +
            "faxNr = @faxNr ," +
            "email = @email ," +
            "latitude = @latitude ," +
            "longitude = @longitude " +
            " where id = @id";

            SQLiteCommand command = GetCommandWithOfficeParameters(office, sql);
            return adaptee.Update(command);
        }
        public int UpdateLoginUser(LoginUser loginUser)
        {
            string sql = "Update " +
             GetEnumDescription(Table.LoginUser) +
             " SET " +
             "username = @username ," +
             "password = @password ," +
             "rights = @rights " +
             " where id = @id";

            SQLiteCommand command = GetCommandWithLoginUserParameters(loginUser, sql);
            return adaptee.Update(command);
        }
        public int UpdateEmployee(Employee employee)
        {
            string sql = "Update " +
            GetEnumDescription(Table.Employee) +
            " SET " +
            "picture = @picture ," +
            "name = @name ," +
            "position = @position ," +
            "email = @email ," +
            "mobileNr = @mobileNr ," +
            "direkteNr = @direkteNr ," +
            "officeId = @officeId ," +
            "loginUserId = @loginUserId " +
            " where id = @id";

            SQLiteCommand command = GetCommandWithEmployeeParameters(employee, sql);
            return adaptee.Update(command);
        }
        public int UpdateBroker(Broker broker)
        {
            string sql = "Update " +
           GetEnumDescription(Table.Broker) +
           " SET " +
           "name = @name ," +
           "email = @email ," +
           "loginUserId = @loginUserId " +
           " where id = @id";

            SQLiteCommand command = GetCommandWithBrokerParameters(broker, sql);
            return adaptee.Update(command);
        }
        public int UpdateHaulier(Haulier haulier)
        {
            string sql = "";

            if (haulier.Broker == null)
            {
                sql = "Update " +
          GetEnumDescription(Table.Haulier) +
          " SET " +
          "name = @name ," +
          "email = @email ," +
          "truckNr = @truckNr ," +
          "loginUserId = @loginUserId " +
          " where id = @id";
            }

            sql = "Update " +
           GetEnumDescription(Table.Haulier) +
           " SET " +
           "name = @name ," +
           "email = @email ," +
           "truckNr = @truckNr ," +
           "brokerId = @brokerId ," +
           "loginUserId = @loginUserId " +
           " where id = @id";

            SQLiteCommand command = GetCommandWithHaulierParameters(haulier, sql);
            return adaptee.Update(command);
        }



        public bool OfficeExists(Office office)
        {
            string sql = "select exists(select 1 from " + GetEnumDescription(Table.Office)
                + " where name = '" + office.Name + "')";
            SQLiteCommand command = new SQLiteCommand(sql);
            int id = adaptee.ExecuteScalar(command);
            return id == 1;
        }
        public bool EmployeeExists(Employee employee)
        {
            string sql = "select exists(select 1 from " + GetEnumDescription(Table.Employee)
                + " where name = '" + employee.Name + "' and email = '" + employee.Email + "')";

            SQLiteCommand command = new SQLiteCommand(sql);
            int id = adaptee.ExecuteScalar(command);
            return id == 1;
        }
        public bool LoginUserExists(LoginUser loginUser)
        {
            string sql = "select exists(select 1 from " + GetEnumDescription(Table.LoginUser)
                + " where username = '" + loginUser.Username + "')";
            SQLiteCommand command = new SQLiteCommand(sql);
            int id = adaptee.ExecuteScalar(command);
            return id == 1;
        }
        public bool BrokerExists(Broker broker)
        {
            string sql = "select exists(select 1 from " + GetEnumDescription(Table.Broker)
              + " where name = '" + broker.Name + "')";
            SQLiteCommand command = new SQLiteCommand(sql);
            int id = adaptee.ExecuteScalar(command);
            return id == 1;
        }
        public bool HaulierExists(Haulier haulier)
        {
            string sql = "select exists(select 1 from " + GetEnumDescription(Table.Haulier)
           + " where name = '" + haulier.Name + "' and truckNr = '" + haulier.TruckNr + "')";
            SQLiteCommand command = new SQLiteCommand(sql);
            int id = adaptee.ExecuteScalar(command);
            return id == 1;
        }
        public bool TruckWashExists(TruckWash truckWash)
        {
            string sql = "select exists(select 1 from " + GetEnumDescription(Table.TruckWash)
           + " where name = '" + truckWash.Name
           + "' and zip = '" + truckWash.Zip
           + "' and latitude = '" + truckWash.Latitude
           + "' and longitude = '" + truckWash.Longitude
           + "')";
            SQLiteCommand command = new SQLiteCommand(sql);
            int id = adaptee.ExecuteScalar(command);
            return id == 1;
        }



        public DataTable AllTruckWash()
        {
            //  return GetAllFromTable(Table.TruckWash);
            return GetAllFromView(View.TruckWash);

        }
        public DataTable AllGasStation()
        {
            // return GetAllFromTable(Table.GasStation);
            return GetAllFromView(View.GasStation);
        }
        public DataTable AllOffice()
        {
            // return GetAllFromTable(Table.Office);
            return GetAllFromView(View.Office);
        }
        public DataTable AllEmployee()
        {
            // return GetAllFromTable(Table.Employee);
            return GetAllFromView(View.EmployeeOfficeLogin);
        }
        public DataTable AllLoginUser()
        {
            return GetAllFromTable(Table.LoginUser);
        }
        public DataTable AllBroker()
        {
            //return GetAllFromTable(Table.Broker);
            return GetAllFromView(View.BrokerLogin);
        }
        public DataTable AllHaulier()
        {
            /// return GetAllFromTable(Table.Haulier);
            return GetAllFromView(View.HaulierBrokerLogin);
        }



        private DataTable GetAllFromTable(Table table)
        {

            string sql = "select * from " + GetEnumDescription(table);
            SQLiteCommand command = new SQLiteCommand(sql);
            DataTable dt = adaptee.Query(command);

            return dt;
        }
        private DataTable GetAllFromView(View view)
        {

            string sql = "select * from " + GetEnumDescription(view);
            SQLiteCommand command = new SQLiteCommand(sql);
            DataTable dt = adaptee.Query(command);

            return dt;
        }
        private DataTable GetAllFromTableWithLoginInfo(Table table)
        {

            string sql = "select * from " + GetEnumDescription(table);
            SQLiteCommand command = new SQLiteCommand(sql);
            DataTable dt = adaptee.Query(command);

            return dt;
        }



        private SQLiteCommand GetCommandWithGasStationParameters(GasStation gasStation, string sql)
        {

            SQLiteCommand command = new SQLiteCommand(sql);
            if (gasStation.HasId)
            {
                SQLiteParameter idParam = new SQLiteParameter("@id", DbType.String);
                idParam.Value = gasStation.Id;
                command.Parameters.Add(idParam);
            }

            SQLiteParameter typeParam = new SQLiteParameter("@type", DbType.String);
            SQLiteParameter nameParam = new SQLiteParameter("@name", DbType.String);
            SQLiteParameter addressParam = new SQLiteParameter("@address", DbType.String);
            SQLiteParameter zipParam = new SQLiteParameter("@zip", DbType.String);
            SQLiteParameter cityParam = new SQLiteParameter("@city", DbType.String);
            SQLiteParameter longitudeParam = new SQLiteParameter("@longitude", DbType.String);
            SQLiteParameter latitudeParam = new SQLiteParameter("@latitude", DbType.String);
            SQLiteParameter adblueParam = new SQLiteParameter("@adblue", DbType.String);

            typeParam.Value = gasStation.Type;
            nameParam.Value = gasStation.Name;
            addressParam.Value = gasStation.Address;
            zipParam.Value = gasStation.Zip;
            cityParam.Value = gasStation.City;
            longitudeParam.Value = gasStation.Longitude;
            latitudeParam.Value = gasStation.Latitude;
            adblueParam.Value = gasStation.AdBlue;

            command.Parameters.Add(typeParam);
            command.Parameters.Add(nameParam);
            command.Parameters.Add(addressParam);
            command.Parameters.Add(zipParam);
            command.Parameters.Add(cityParam);
            command.Parameters.Add(longitudeParam);
            command.Parameters.Add(latitudeParam);
            command.Parameters.Add(adblueParam);
            return command;
        }
        private SQLiteCommand GetCommandWithTruckWashParameters(TruckWash truckWash, string sql)
        {
            SQLiteCommand command = new SQLiteCommand(sql);
            if (truckWash.HasId)
            {
                SQLiteParameter idParam = new SQLiteParameter("@id", DbType.String);
                idParam.Value = truckWash.Id;
                command.Parameters.Add(idParam);
            }

            SQLiteParameter nameParam = new SQLiteParameter("@name", DbType.String);
            SQLiteParameter addressParam = new SQLiteParameter("@address", DbType.String);
            SQLiteParameter zipParam = new SQLiteParameter("@zip", DbType.String);
            SQLiteParameter cityParam = new SQLiteParameter("@city", DbType.String);
            SQLiteParameter phoneParam = new SQLiteParameter("@phone", DbType.String);
            SQLiteParameter emailParam = new SQLiteParameter("@email", DbType.String);
            SQLiteParameter openingHoursParam = new SQLiteParameter("@openingHours", DbType.String);
            SQLiteParameter truckStandardPrice = new SQLiteParameter("@truckStandardPrice", DbType.String);
            SQLiteParameter trailerStandardPrice = new SQLiteParameter("@trailerStandardPrice", DbType.String);
            SQLiteParameter truckTrailerStandardPrice = new SQLiteParameter("@truckTrailerStandardPrice", DbType.String);
            SQLiteParameter truckQuickWashPrice = new SQLiteParameter("@truckQuickWashPrice", DbType.String);
            SQLiteParameter trailerQuickWashPrice = new SQLiteParameter("@trailerQuickWashPrice", DbType.String);
            SQLiteParameter truckTrailerQuickWashPrice = new SQLiteParameter("@truckTrailerQuickWashPrice", DbType.String);
            SQLiteParameter remarksParam = new SQLiteParameter("@remarks", DbType.String);
            SQLiteParameter websiteParam = new SQLiteParameter("@website", DbType.String);
            SQLiteParameter latitudeParam = new SQLiteParameter("@latitude", DbType.String);
            SQLiteParameter longitudeParam = new SQLiteParameter("@longitude", DbType.String);

            nameParam.Value = truckWash.Name;
            addressParam.Value = truckWash.Address;
            zipParam.Value = truckWash.Zip;
            cityParam.Value = truckWash.City;
            phoneParam.Value = truckWash.Phone;
            emailParam.Value = truckWash.Email;
            openingHoursParam.Value = truckWash.OpeningHours;
            truckStandardPrice.Value = truckWash.TruckStandardPrice;
            trailerStandardPrice.Value = truckWash.TrailerStandardPrice;
            truckTrailerStandardPrice.Value = truckWash.TruckTrailerStandardPrice;
            truckQuickWashPrice.Value = truckWash.TruckQuickWashPrice;
            trailerQuickWashPrice.Value = truckWash.TrailerQuickWashPrice;
            truckTrailerQuickWashPrice.Value = truckWash.TruckTrailerQuickWashPrice;
            remarksParam.Value = truckWash.Remarks;
            websiteParam.Value = truckWash.Website;
            latitudeParam.Value = truckWash.Latitude;
            longitudeParam.Value = truckWash.Longitude;


            command.Parameters.Add(nameParam);
            command.Parameters.Add(addressParam);
            command.Parameters.Add(zipParam);
            command.Parameters.Add(cityParam);
            command.Parameters.Add(phoneParam);
            command.Parameters.Add(emailParam);
            command.Parameters.Add(openingHoursParam);
            command.Parameters.Add(truckStandardPrice);
            command.Parameters.Add(trailerStandardPrice);
            command.Parameters.Add(truckTrailerStandardPrice);
            command.Parameters.Add(truckQuickWashPrice);
            command.Parameters.Add(trailerQuickWashPrice);
            command.Parameters.Add(truckTrailerQuickWashPrice);
            command.Parameters.Add(remarksParam);
            command.Parameters.Add(websiteParam);
            command.Parameters.Add(latitudeParam);
            command.Parameters.Add(longitudeParam);

            return command;
        }
        private SQLiteCommand GetCommandWithOfficeParameters(Office office, string sql)
        {
            SQLiteCommand command = new SQLiteCommand(sql);
            if (office.HasId)
            {
                SQLiteParameter idParam = new SQLiteParameter("@id", DbType.String);
                idParam.Value = office.Id;
                command.Parameters.Add(idParam);
            }

            SQLiteParameter nameParam = new SQLiteParameter("@name", DbType.String);
            SQLiteParameter addressParam = new SQLiteParameter("@address", DbType.String);
            SQLiteParameter zipCodeParam = new SQLiteParameter("@zipCode", DbType.String);
            SQLiteParameter cityParam = new SQLiteParameter("@city", DbType.String);
            SQLiteParameter phoneNrParam = new SQLiteParameter("@phoneNr", DbType.String);
            SQLiteParameter faxNrParam = new SQLiteParameter("@faxNr", DbType.String);
            SQLiteParameter emailParam = new SQLiteParameter("@email", DbType.String);
            SQLiteParameter latitudeParam = new SQLiteParameter("@latitude", DbType.String);
            SQLiteParameter longitudeParam = new SQLiteParameter("@longitude", DbType.String);

            nameParam.Value = office.Name;
            addressParam.Value = office.Address;
            zipCodeParam.Value = office.ZipCode;
            cityParam.Value = office.City;
            phoneNrParam.Value = office.PhoneNr;
            faxNrParam.Value = office.FaxNr;
            emailParam.Value = office.Email;
            latitudeParam.Value = office.Latitude;
            longitudeParam.Value = office.Longitude;

            command.Parameters.Add(nameParam);
            command.Parameters.Add(addressParam);
            command.Parameters.Add(zipCodeParam);
            command.Parameters.Add(cityParam);
            command.Parameters.Add(phoneNrParam);
            command.Parameters.Add(faxNrParam);
            command.Parameters.Add(emailParam);
            command.Parameters.Add(latitudeParam);
            command.Parameters.Add(longitudeParam);

            return command;


        }
        private SQLiteCommand GetCommandWithLoginUserParameters(LoginUser loginUser, string sql)
        {
            SQLiteCommand command = new SQLiteCommand(sql);
            if (loginUser.HasId)
            {
                SQLiteParameter idParam = new SQLiteParameter("@id", DbType.String);
                idParam.Value = loginUser.Id;
                command.Parameters.Add(idParam);
            }

            SQLiteParameter usernameParam = new SQLiteParameter("@username", DbType.String);
            SQLiteParameter passwordParam = new SQLiteParameter("@password", DbType.String);
            SQLiteParameter rightsParam = new SQLiteParameter("@rights", DbType.String);

            usernameParam.Value = loginUser.Username;
            passwordParam.Value = loginUser.Password;
            rightsParam.Value = Utilities.GetEnumDescription(loginUser.Rights);

            command.Parameters.Add(usernameParam);
            command.Parameters.Add(passwordParam);
            command.Parameters.Add(rightsParam);

            return command;
        }
        private SQLiteCommand GetCommandWithEmployeeParameters(Employee employee, string sql)
        {
            SQLiteCommand command = new SQLiteCommand(sql);
            if (employee.HasId)
            {
                SQLiteParameter idParam = new SQLiteParameter("@id", DbType.String);
                idParam.Value = employee.Id;
                command.Parameters.Add(idParam);
            }

            SQLiteParameter pictureParam = new SQLiteParameter("@picture", DbType.Binary);
            SQLiteParameter nameParam = new SQLiteParameter("@name", DbType.String);
            SQLiteParameter positionParam = new SQLiteParameter("@position", DbType.String);
            SQLiteParameter emailParam = new SQLiteParameter("@email", DbType.String);
            SQLiteParameter mobileNrParam = new SQLiteParameter("@mobileNr", DbType.String);
            SQLiteParameter direkteNrParam = new SQLiteParameter("@direkteNr", DbType.String);
            SQLiteParameter officeIdParam = new SQLiteParameter("@officeId", DbType.String);
            SQLiteParameter loginUserParam = new SQLiteParameter("@loginUserId", DbType.String);


            pictureParam.Value = employee.Picture;
            nameParam.Value = employee.Name;
            positionParam.Value = employee.Position;
            emailParam.Value = employee.Email;
            mobileNrParam.Value = employee.MobileNr;
            direkteNrParam.Value = employee.DirekteNr;

            //First Check for the id of the Office in the DB
            int officeID = GetOfficeId(employee.Office);
            officeIdParam.Value = officeID;

            if (employee.HasLoginUser())
            {
                AddLoginUser(employee.LoginUser);
                int loginUserId = GetLoginUserId(employee.LoginUser);
                loginUserParam.Value = loginUserId;
            }

            else
                loginUserParam.Value = employee.LoginUser;

            command.Parameters.Add(pictureParam);
            command.Parameters.Add(nameParam);
            command.Parameters.Add(positionParam);
            command.Parameters.Add(emailParam);
            command.Parameters.Add(mobileNrParam);
            command.Parameters.Add(direkteNrParam);
            command.Parameters.Add(officeIdParam);
            command.Parameters.Add(loginUserParam);

            return command;
        }
        private SQLiteCommand GetCommandWithBrokerParameters(Broker broker, string sql)
        {
            SQLiteCommand command = new SQLiteCommand(sql);
            if (broker.HasId)
            {
                SQLiteParameter idParam = new SQLiteParameter("@id", DbType.String);
                idParam.Value = broker.Id;
                command.Parameters.Add(idParam);
            }

            SQLiteParameter nameParam = new SQLiteParameter("@name", DbType.String);
            SQLiteParameter emailParam = new SQLiteParameter("@email", DbType.String);
            SQLiteParameter loginUserIdParam = new SQLiteParameter("@loginUserId", DbType.String);


            nameParam.Value = broker.Name;
            emailParam.Value = broker.Email;

            AddLoginUser(broker.LoginUser);
            int loginUserId = GetLoginUserId(broker.LoginUser);
            loginUserIdParam.Value = loginUserId;

            command.Parameters.Add(nameParam);
            command.Parameters.Add(emailParam);
            command.Parameters.Add(loginUserIdParam);

            return command;


        }
        private SQLiteCommand GetCommandWithHaulierParameters(Haulier haulier, string sql)
        {
            SQLiteCommand command = new SQLiteCommand(sql);
            if (haulier.HasId)
            {
                SQLiteParameter idParam = new SQLiteParameter("@id", DbType.String);
                idParam.Value = haulier.Id;
                command.Parameters.Add(idParam);
            }

            SQLiteParameter nameParam = new SQLiteParameter("@name", DbType.String);
            SQLiteParameter emailParam = new SQLiteParameter("@email", DbType.String);
            SQLiteParameter truckNrParam = new SQLiteParameter("@truckNr", DbType.String);
            SQLiteParameter brokerIdParam = new SQLiteParameter("@brokerId", DbType.String);
            SQLiteParameter loginUserIdParam = new SQLiteParameter("@loginUserId", DbType.String);



            nameParam.Value = haulier.Name;
            emailParam.Value = haulier.Email;
            truckNrParam.Value = haulier.TruckNr;

            if (haulier.HasBroker())
            {
                int brokerId = GetBrokerId(haulier.Broker);
                brokerIdParam.Value = brokerId;
                command.Parameters.Add(brokerIdParam);
            }
            else
                brokerIdParam.Value = haulier.Broker;

            AddLoginUser(haulier.LoginUser);
            int loginUserId = GetLoginUserId(haulier.LoginUser);
            loginUserIdParam.Value = loginUserId;


            command.Parameters.Add(nameParam);
            command.Parameters.Add(emailParam);
            command.Parameters.Add(truckNrParam);
            command.Parameters.Add(loginUserIdParam);

            return command;
        }

        private string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }


        private int GetOfficeId(Office office)
        {
            string sql = "select id from " + GetEnumDescription(Table.Office) + " where name = " + "'" + office.Name + "'";
            SQLiteCommand command = new SQLiteCommand(sql);
            int id = adaptee.ExecuteScalar(command);
            return id;
        }
        private int GetLoginUserId(LoginUser loginUser)
        {
            string sql = "select id from " + GetEnumDescription(Table.LoginUser) + " where username = " + "'" + loginUser.Username + "'";
            SQLiteCommand command = new SQLiteCommand(sql);
            int id = adaptee.ExecuteScalar(command);
            return id;
        }
        private int GetBrokerId(Broker broker)
        {
            string sql = "select id from " + GetEnumDescription(Table.Broker) + " where name = " + "'" + broker.Name + "'";
            SQLiteCommand command = new SQLiteCommand(sql);
            int id = adaptee.ExecuteScalar(command);
            return id;
        }


    }
}
