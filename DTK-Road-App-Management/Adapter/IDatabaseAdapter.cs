﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    public enum Table
    {
        [Description("Gas_Station")]
        GasStation,
        [Description("Truck_Wash")]
        TruckWash,
        [Description("Office")]
        Office,
        [Description("Employee")]
        Employee,
        [Description("LoginUser")]
        LoginUser,
        [Description("Broker")]
        Broker,
        [Description("Haulier")]
        Haulier
    }
    public enum View
    {
        [Description("truck_wash_view")]
        TruckWash,
        [Description("gas_station_view")]
        GasStation,
        [Description("office_view")]
        Office,
        [Description("employee_office_login_view")]
        EmployeeOfficeLogin,
        [Description("haulier_broker_login_view")]
        HaulierBrokerLogin,
        [Description("broker_login_view")]
        BrokerLogin
    }

    public interface IDatabaseAdapter
    {
        int AddTruckWash(TruckWash truckWash);
        int AddGasStation(GasStation gasStation);
        int AddOffice(Office office);
        int AddEmployee(Employee employee);
        int AddLoginUser(LoginUser loginUser);
        int AddBroker(Broker broker);
        int AddHaulier(Haulier haulier);



        int UpdateTruckWash(TruckWash truckwash);
        int UpdateGasStation(GasStation gasStation);
        int UpdateOffice(Office office);
        int UpdateEmployee(Employee employee);
        int UpdateLoginUser(LoginUser loginUser);
        int UpdateBroker(Broker broker);
        int UpdateHaulier(Haulier haulier);


        bool TruckWashExists(TruckWash truckWash);
        bool OfficeExists(Office office);
        bool EmployeeExists(Employee employee);
        bool LoginUserExists(LoginUser loginUser);
        bool BrokerExists(Broker broker);
        bool HaulierExists(Haulier haulier);


        int DeleteAllFromTable(Table table);
        int DeleteFromTableById(Table table, int id);
        int DeleteFromTableByColumn(Table table, string column, int id);
        DataTable AllTruckWash();
        DataTable AllGasStation();
        DataTable AllOffice();
        DataTable AllEmployee();
        DataTable AllLoginUser();
        DataTable AllBroker();
        DataTable AllHaulier();

    }
}
