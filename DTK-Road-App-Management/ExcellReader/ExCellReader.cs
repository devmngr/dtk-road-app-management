﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;           //microsoft Excel 16 object in references-> COM tab

namespace ExcellReader
{
    class ExCellReader
    {
        private char valueSeparation = ';';
        private List<string> csvFile;


        public ExCellReader()
        {
            csvFile = new List<string>();
        }

        public List<string> readCsvFile(string filePath)
        {
            var reader = new StreamReader(File.OpenRead(filePath), System.Text.Encoding.GetEncoding("iso-8859-1"));
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                line = line.Remove(line.Length - 1);
                csvFile.Add(line);
            }
            reader.Close();
            return csvFile;
        }

        public string[] readLineValues(int index)
        {
            string[] values = csvFile[index].Split(valueSeparation).Select(sValue => sValue.Trim()).ToArray();
            return values;
        }

        public int numberOfLines()
        {
            return csvFile.Count();
        }
        public int numberOfValuesInLine(int index)
        {
            string[] values = readLineValues(index);
            return values.Length;
        }

    }
}
