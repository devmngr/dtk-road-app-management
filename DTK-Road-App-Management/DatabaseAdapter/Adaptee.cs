﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAdapter
{
    class Adaptee
    {

        private SQLiteConnection connection;

        public Adaptee()
        {

        }

        public Adaptee(string path)
        {
            connection = new SQLiteConnection(path);
        }

        public void openDatabase()
        {
            connection.Open();
        }
        public void closeDatabase()
        {
            connection.Close();
        }

        public void update(SQLiteCommand command)
        {
            command.Connection = connection;

            openDatabase();
            command.Prepare();
            command.ExecuteNonQuery();
            closeDatabase();
        }

        public SQLiteDataReader query(SQLiteCommand command)
        {
            command.Connection = connection;
            openDatabase();
            command.Prepare();
            SQLiteDataReader reader = command.ExecuteReader();
            closeDatabase();
            return reader;
        }


        public void createNewDatabase(string databaseName, string path)
        {
            string temp = path + databaseName + ".sqlite";
            SQLiteConnection.CreateFile(temp);
        }
    }
}
