﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAdapter
{
    class DatabaseInitialisation
    {
        public void createDatabase(string path)
        {
            SQLiteConnection.CreateFile(path);
        }
    }
}
