﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAdapter
{
    class DbAdapter
    {

        private Adaptee adaptee;


        public DbAdapter(string path)
        {
            adaptee = new Adaptee(path);

        }


        /*
                public void create(string str)
                {
                    connectDB(str);
                    openDB();
                    string sql = "create table high (name varchar(20), score int)";
                    SQLiteCommand command = new SQLiteCommand(sql, conn);
                    command.ExecuteNonQuery();
                    sql = "insert into high (name, score) values ('Me', 3000)";
                    command = new SQLiteCommand(sql, conn);
                    command.ExecuteNonQuery();
                    closeDB();
                }*/

        public void insert()
        {
            string str = "Data Source=D:/Hello.sqlite;Version=3;";
            Adaptee adaptee = new Adaptee(str);
            string sql = "insert into highscores (name,score)" +
                            "values(@name,@score)";

            SQLiteCommand command = new SQLiteCommand(sql);

            SQLiteParameter nameParam = new SQLiteParameter("@name", DbType.String, 0);
            SQLiteParameter scoreParam = new SQLiteParameter("@score", DbType.Int32, 1);

            nameParam.Value = "ME";
            scoreParam.Value = "20";

            command.Parameters.Add(nameParam);
            command.Parameters.Add(scoreParam);

            adaptee.update(command);
        }

        public void delete()
        {

            string str = "Data Source=D:/Hello.sqlite;Version=3;";
            Adaptee adaptee = new Adaptee(str);
            string sql = "delete from highscores";
            SQLiteCommand command = new SQLiteCommand(sql);
            adaptee.update(command);

        }


        public void update()
        {
            string str = "Data Source=D:/Hello.sqlite;Version=3;";
            Adaptee adaptee = new Adaptee(str);
            string sql = "update highscores set name = 'ABC' where score = 40";
            SQLiteCommand command = new SQLiteCommand(sql);
            adaptee.update(command);
        }

        public void display()
        {




        }


    }
}
