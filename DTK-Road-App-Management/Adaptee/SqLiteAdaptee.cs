﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adaptee
{
    public class SqLiteAdaptee
    {
        private SQLiteConnection connection;

        private static string URL = "Data Source = D:/DTK.db;Version=3;";

        public SqLiteAdaptee()
        {
            this.connection = new SQLiteConnection(URL);
        }

        public SqLiteAdaptee(string connectionString)
        {
            this.connection = new SQLiteConnection(connectionString);
        }

        public void OpenDatabase()
        {
            connection.Open();
        }
        public void CloseDatabase()
        {
            connection.Close();
        }

        public int Update(SQLiteCommand command)
        {
            int temp = 0;

            command.Connection = connection;

            OpenDatabase();
            command.Prepare();
            temp = command.ExecuteNonQuery();
            CloseDatabase();

            return temp;
        }
        public int ExecuteScalar(SQLiteCommand command)
        {
            Int32 temp = 0;

            command.Connection = connection;

            OpenDatabase();
            command.Prepare();
            temp = Convert.ToInt32(command.ExecuteScalar());
            CloseDatabase();

            return (int)temp;
        }

        public DataTable Query(SQLiteCommand command)
        {
            DataTable dt = new DataTable();
            command.Connection = connection;
            OpenDatabase();
            command.Prepare();
            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                dt.Load(reader);
                reader.Close();

            }
            CloseDatabase();
            return dt;

        }
    }
}
