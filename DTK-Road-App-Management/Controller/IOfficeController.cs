﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controller
{
    public interface IOfficeController
    {
        int AddOffice(Office office);
        int UpdateOffice(Office office);
        int DeleteOffice(Office office);
        void FillOfficeGrid(DataGridView dataGridView);


    }
}
