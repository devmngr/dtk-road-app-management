﻿using ModelManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controller
{
    public class LoginUserControllerImpl : ILoginUserController
    {
        private IModelManager manager;

        public LoginUserControllerImpl()
        {
            manager = new ModelManagerImpl();
        }
        public void FillLoginUserGrid(DataGridView dataGridView)
        {
            BindingSource bindingSource = new BindingSource();
            DataTable dataTable = manager.AllLoginUser();
            bindingSource.DataSource = dataTable;
            dataGridView.DataSource = bindingSource;
        }
    }
}
