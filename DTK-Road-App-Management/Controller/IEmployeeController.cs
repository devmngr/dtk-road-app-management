﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
namespace Controller
{
    public interface IEmployeeController
    {
        int AddEmployee(Employee employee);
        int UpdateEmployee(Employee employee);
        int DeleteEmployee(Employee employee);
        void FillEmployeeGrid(DataGridView dataGridView);
        DataTable AllOffice();
    }
}
