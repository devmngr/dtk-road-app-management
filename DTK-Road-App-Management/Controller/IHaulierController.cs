﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
namespace Controller
{
    public interface IHaulierController
    {

        int AddHaulier(Haulier haulier);
        int UpdateHaulier(Haulier haulier);
        int DeleteHaulier(Haulier haulier);
        void FillHaulierGrid(DataGridView dataGridView);
        DataTable AllBroker();
    }
}
