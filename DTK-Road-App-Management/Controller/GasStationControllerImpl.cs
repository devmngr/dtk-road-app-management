﻿using Model;
using System.Collections.Generic;
using ModelManager;
using System.Windows.Forms;
using Csv;
using System.Linq;
using System.Data;

namespace Controller
{
    public class GasStationControllerImpl : IGasStationController
    {
        private IModelManager manager;
        private static string[] columnNames =
    {"Type", "Name", "Address", "Zip", "City","AdBLUE", "Longitude", "Latitude"  };
        private static string[] hasAdblueArr = { "YES", "JA", "Y", "J", "1" };
        // private List<GasStation> gasStations;
        private CsvReader csvReader;
        public GasStationControllerImpl()
        {
            manager = new ModelManagerImpl();
        }
        public void FillGasStationGrid(DataGridView dataGridView)
        {
            BindingSource bindingSource = new BindingSource();
            DataTable dataTable = manager.AllGasStation();
            bindingSource.DataSource = dataTable;
            dataGridView.DataSource = bindingSource;
        }
        public void CsvToDataGridView(DataGridView view, DataTable dt)
        {
            BindingSource bindingSource = new BindingSource();
            bindingSource.DataSource = dt;
            view.DataSource = bindingSource;
        }
        public DataTable DataTableFromCsv(string filePath, char delimiter)
        {
            csvReader = new CsvReader(delimiter);
            return csvReader.CsvToDataTable(filePath, columnNames);
        }
        public int AddGasStation(GasStation gasStation)
        {
            return manager.AddGasStation(gasStation);
        }

        public int DeleteAllFromGasStation()
        {
            return manager.DeleteAllFromGasStation();
        }


        /* public DataTable CsvToDataTable(string filePath)
         {
             DataTable dt = new DataTable();

             CsvReader reader = new CsvReader();
             reader.readCsvFile(filePath);
             string[] arr = null;

             for (int i = 0; i < reader.numberOfLines(); i++)
             {
                 arr = reader.readLineValues(i);

                 gasStations.Add(GasStationFromArray(arr));
                 item = new ListViewItem(arr);
                 listView.Items.Add(item);

             }

             return dt;
         }


         /*
                 public int SaveToDatabase()
                 {
                     int i = 0;
                     manager.DeleteAllFromGasStation();
                     foreach (GasStation gas in gasStations)
                         i += manager.AddGasStation(gas);

                     gasStations.Clear();
                     return i;
                 }





                 private void LoadColumsTitle(ListView listView, string[] arr)
                 {
                     for (int i = 0; i < arr.Length; i++)
                     {
                         listView.Columns.Add(arr[i], 150);
                     }
                 }
                 private GasStation GasStationFromArray(string[] arr)
                 {
                     string type = arr[0];
                     string name = arr[1];
                     string address = arr[2];
                     string zip = arr[3];
                     string city = arr[4];
                     int adBlue = 0;
                     string longitude = arr[6];
                     string latitude = arr[7];


                     if (HasAdblue(arr[5]))
                         adBlue = 1;

                     return new GasStation(type, name, address, zip, city, longitude, latitude, adBlue);
                 }


                 private bool HasAdblue(string input)
                 {
                     foreach (string str in hasAdblueArr)
                         if (input.ToUpper().Equals(str.ToUpper()))
                             return true;
                     return false;
                 }

                 private bool IsGasStationStructure(string[] arr)
                 {
                     return arr.SequenceEqual(columnNames);
                 }

             */
    }
}

/* public void DatabaseToListView(ListView listView)
 {
     LoadColumsTitle(listView, columnNames);
     List<GasStation> list = manager.AllGasStation();
     foreach (GasStation gas in list)
     {
         ListViewItem item = new ListViewItem(gas.GasStationToArray());
         listView.Items.Add(item);
     }
 }
public void CsvToListView(string filePath, ListView listView)
{
    gasStations = new List<GasStation>();

    CsvReader reader = new CsvReader();
    reader.readCsvFile(filePath);
    string[] arr = null;
    ListViewItem item = null;

    for (int i = 0; i < reader.numberOfLines(); i++)
    {
        arr = reader.readLineValues(i);
        if (i == 0)
        {
            LoadColumsTitle(listView, arr);
        }
        else
        {
            gasStations.Add(GasStationFromArray(arr));
            item = new ListViewItem(arr);
            listView.Items.Add(item);
        }
    }
}*/
