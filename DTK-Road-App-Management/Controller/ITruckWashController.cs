﻿using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controller
{
    public interface ITruckWashController
    {
        //   void DatabaseToListView(ListView listView);
        // void CsvToListView(string filePath, ListView listView);

        //   int SaveToDatabase();
        int AddNewTruckWash(TruckWash truckWash);
        int UpdateTruckWash(TruckWash truckWash);
        int DeleteTruckWash(TruckWash truckWash);
        void FillTruckWashGrid(DataGridView dataGridView);
    }
}
