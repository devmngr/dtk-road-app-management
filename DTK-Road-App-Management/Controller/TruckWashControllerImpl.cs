﻿using ModelManager;
using System.Collections.Generic;
using System.Windows.Forms;
using Model;
using Csv;
using System.Linq;
using System.Data;

namespace Controller
{
    public class TruckWashControllerImpl : ITruckWashController
    {
        private IModelManager manager;
        //  private List<TruckWash> truckWashes;
        private static string[] columnNames ={ "ID","Name", "Address", "Zip", "City", "Phone", "Email", "Opening Hours",
            "Truck Standard Price","Trailer Standard Price", "Truck+Trailer Standard Price",
            "Truck Quick Wash Price","Trailer Quick Wash Price","Truck+Trailer Quick Wash Price",
            "Remarks", "Website","Longitude", "Latitude"};


        public TruckWashControllerImpl()
        {
            manager = new ModelManagerImpl();
        }



        public int AddNewTruckWash(TruckWash truckWash)
        {
            return manager.AddTruckWash(truckWash);
        }
        public int UpdateTruckWash(TruckWash truckWash)
        {
            return manager.UpdateTruckWash(truckWash);
        }

        public int DeleteTruckWash(TruckWash truckWash)
        {
            return manager.DeleteTruckWash(truckWash);
        }

        public void FillTruckWashGrid(DataGridView dataGridView)
        {
            BindingSource bindingSource = new BindingSource();
            DataTable dataTable = manager.AllTruckWash();
            bindingSource.DataSource = dataTable;
            dataGridView.DataSource = bindingSource;
        }


        /*  public void CsvToListView(string filePath, ListView listView)
          {
              truckWashes = new List<TruckWash>();

              CsvReader reader = new CsvReader();
              reader.readCsvFile(filePath);
              string[] arr = null;
              ListViewItem item = null;

              for (int i = 0; i < reader.numberOfLines(); i++)
              {
                  arr = reader.readLineValues(i);
                  if (i == 0)
                  {
                      LoadColumsTitle(listView, arr);

                  }
                  else
                  {
                      truckWashes.Add(TruckWashFromArray(arr));
                      item = new ListViewItem(arr);
                      listView.Items.Add(item);
                  }
              }
              //  return 1;
          }

            public void DatabaseToListView(ListView listView)
            {
                LoadColumsTitle(listView, columnNames);
                List<TruckWash> list = manager.AllTruckWash();
                foreach (TruckWash tr in list)
                {
                    ListViewItem item = new ListViewItem(tr.TruckWashToArray());
                    listView.Items.Add(item);
                }
            }





        public int SaveToDatabase()
        {
            int i = 0;
            manager.DeleteAllFromTruckWash();
            foreach (TruckWash tr in truckWashes)
                i += manager.AddTruckWash(tr);

            truckWashes.Clear();
            return i;
        }



        private TruckWash TruckWashFromArray(string[] arr)
        {
            string name = arr[0];
            string address = arr[1];
            string zip = arr[2];
            string city = arr[3];
            string phone = arr[4];
            string email = arr[5];
            string openingHours = arr[6];
            string truckStandardPrice = arr[7];
            string trailerStandardPrice = arr[8];
            string truckTrailerStandardPrice = arr[9];
            string truckQuickWashPrice = arr[10];
            string trailerQuickWashPice = arr[11];
            string truckTrailerQuickWashPrice = arr[12];
            string remarks = arr[13];
            string website = arr[14];
            string longitude = arr[15];
            string latitude = arr[16];

            return new TruckWash(name, address, zip, city, phone, email, openingHours,
                truckStandardPrice, trailerStandardPrice, truckTrailerStandardPrice,
                truckQuickWashPrice, trailerQuickWashPice, truckTrailerQuickWashPrice,
                remarks, website, longitude, latitude);

        }
        private void LoadColumsTitle(ListView listView, string[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                listView.Columns.Add(arr[i], 150);
            }
        }

        private bool IsTruckWashStructure(string[] arr)
        {
            return arr.SequenceEqual(columnNames);
        }

        */
    }
}
