﻿using Model;
using ModelManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controller
{
    public class BrokerControllerImpl : IBrokerController
    {
        private IModelManager manager;

        public BrokerControllerImpl()
        {
            manager = new ModelManagerImpl();
        }

        public int AddBroker(Broker broker)
        {
            return manager.AddBroker(broker);
        }

        public int DeleteBroker(Broker broker)
        {
            manager.DeleteAllHaulierOfBroker(broker);
            return manager.DeleteBroker(broker);
        }

        public int UpdateBroker(Broker broker)
        {
            return manager.UpdateBroker(broker);
        }

        public void FillBrokerGrid(DataGridView dataGridView)
        {
            BindingSource bindingSource = new BindingSource();
            DataTable dataTable = manager.AllBroker();
            bindingSource.DataSource = dataTable;
            dataGridView.DataSource = bindingSource;
        }

    }
}
