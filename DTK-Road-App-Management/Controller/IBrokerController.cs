﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
namespace Controller
{
    public interface IBrokerController
    {

        int AddBroker(Broker broker);
        int UpdateBroker(Broker broker);
        int DeleteBroker(Broker broker);

        void FillBrokerGrid(DataGridView dataGridView);
    }
}
