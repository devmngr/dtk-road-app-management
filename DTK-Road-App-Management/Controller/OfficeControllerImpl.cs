﻿using Model;
using ModelManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controller
{
    public class OfficeControllerImpl : IOfficeController
    {

        private IModelManager manager;

        public OfficeControllerImpl()
        {
            manager = new ModelManagerImpl();
        }

        public int AddOffice(Office office)
        {
            return manager.AddOffice(office);
        }
        public int UpdateOffice(Office office)
        {
            return manager.UpdateOffice(office);
        }

        public int DeleteOffice(Office office)
        {
            return manager.DeleteOffice(office);
        }
        public void FillOfficeGrid(DataGridView dataGridView)
        {
            BindingSource bindingSource = new BindingSource();
            DataTable dataTable = manager.AllOffice();
            bindingSource.DataSource = dataTable;
            dataGridView.DataSource = bindingSource;
        }
    }
}
