﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
namespace Controller
{
    public interface IGasStationController
    {
        //  void DatabaseToListView(ListView listView);
        // void CsvToListView(string filePath, ListView listView);

        DataTable DataTableFromCsv(string filePath, char delimiter);
        void CsvToDataGridView(DataGridView view, DataTable dt);
        void FillGasStationGrid(DataGridView dataGridView);
        int AddGasStation(GasStation gasStation);
        int DeleteAllFromGasStation();
    }
}
