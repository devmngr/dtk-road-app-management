﻿using Model;
using ModelManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controller
{
    public class HaulierControllerImpl : IHaulierController
    {
        private IModelManager manager;

        public HaulierControllerImpl()
        {
            manager = new ModelManagerImpl();
        }

        public int AddHaulier(Haulier haulier)
        {
            return manager.AddHaulier(haulier);
        }

        public int DeleteHaulier(Haulier haulier)
        {
            return manager.DeleteHaulier(haulier);
        }

        public int UpdateHaulier(Haulier haulier)
        {
            return manager.UpdateHaulier(haulier);
        }

        public void FillHaulierGrid(DataGridView dataGridView)
        {
            BindingSource bindingSource = new BindingSource();
            DataTable dataTable = manager.AllHaulier();
            bindingSource.DataSource = dataTable;
            dataGridView.DataSource = bindingSource;
        }

        public DataTable AllBroker()
        {
            return manager.AllBroker();
        }
    }
}
