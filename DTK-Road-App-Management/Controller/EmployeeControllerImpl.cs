﻿using Model;
using ModelManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controller
{
    public class EmployeeControllerImpl : IEmployeeController
    {
        private IModelManager manager;

        public EmployeeControllerImpl()
        {
            manager = new ModelManagerImpl();
        }

        public int AddEmployee(Employee employee)
        {
            return manager.AddEmployee(employee);
        }

        public DataTable AllOffice()
        {
            return manager.AllOffice();
        }

        public int DeleteEmployee(Employee employee)
        {
            return manager.DeleteEmployee(employee);
        }

        public void FillEmployeeGrid(DataGridView dataGridView)
        {
            BindingSource bindingSource = new BindingSource();
            DataTable dataTable = manager.AllEmployee();
            bindingSource.DataSource = dataTable;
            dataGridView.DataSource = bindingSource;
        }

        public int UpdateEmployee(Employee employee)
        {
            return manager.UpdateEmployee(employee);
        }
    }
}
