﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace View
{
    public partial class GasStationsFromCsv : Form
    {
        DataTable dt;
        IGasStationController controller;
        HomeForm homeForm;
        public GasStationsFromCsv(HomeForm homeForm, DataTable dt, IGasStationController controller)
        {
            InitializeComponent();
            this.homeForm = homeForm;
            this.controller = controller;
            this.dt = dt;

            if (dt != null)
                LoadData(dt);
        }
        private void LoadData(DataTable dt)
        {
            BindingSource bindingSource = new BindingSource();
            bindingSource.DataSource = dt;
            gsCSV_dataGridView.DataSource = bindingSource;
        }

        private void SaveToDatabase()
        {
            GasStation temp = null;
            for (int i = 0; i < gsCSV_dataGridView.Rows.Count; i++)
            {
                temp = RowToGasStation(gsCSV_dataGridView.Rows[i]);
                controller.AddGasStation(temp);
            }
        }
        private GasStation RowToGasStation(DataGridViewRow row)
        {
            string type = row.Cells[ViewColumnNames.type_str].FormattedValue.ToString();
            string name = row.Cells[ViewColumnNames.name_str].FormattedValue.ToString();
            string address = row.Cells[ViewColumnNames.address_str].FormattedValue.ToString();
            string zip = row.Cells[ViewColumnNames.zip_str].FormattedValue.ToString();
            string city = row.Cells[ViewColumnNames.city_str].FormattedValue.ToString();
            string adBlue = row.Cells[ViewColumnNames.adblue_str].FormattedValue.ToString();
            string longitude = row.Cells[ViewColumnNames.longitude_str].FormattedValue.ToString();
            string latiude = row.Cells[ViewColumnNames.latitude_str].FormattedValue.ToString();

            return new GasStation(type, name, address, zip, city, longitude, latiude, adBlue);
        }

        private void gsCSV_save_btn_Click(object sender, EventArgs e)
        {
            int temp = controller.DeleteAllFromGasStation();
            SaveToDatabase();
            homeForm.UpdateGasStationDataGridView();
            Close();
        }

        private void gsCSV_back_btn_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
