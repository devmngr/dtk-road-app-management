﻿namespace View
{
    partial class NewOffice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.new_office_name_txt = new System.Windows.Forms.TextBox();
            this.new_office_address_txt = new System.Windows.Forms.TextBox();
            this.new_office_zip_txt = new System.Windows.Forms.TextBox();
            this.new_office_city_txt = new System.Windows.Forms.TextBox();
            this.new_office_phone_txt = new System.Windows.Forms.TextBox();
            this.new_office_fax_txt = new System.Windows.Forms.TextBox();
            this.new_office_email_txt = new System.Windows.Forms.TextBox();
            this.new_office_latitude_txt = new System.Windows.Forms.TextBox();
            this.new_office_longitude_txt = new System.Windows.Forms.TextBox();
            this.new_office_back_btn = new System.Windows.Forms.Button();
            this.new_office_save_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(40, 116);
            this.label2.Margin = new System.Windows.Forms.Padding(20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 39);
            this.label2.TabIndex = 1;
            this.label2.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(40, 194);
            this.label3.Margin = new System.Windows.Forms.Padding(20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 39);
            this.label3.TabIndex = 2;
            this.label3.Text = "Zip";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(40, 272);
            this.label4.Margin = new System.Windows.Forms.Padding(20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 39);
            this.label4.TabIndex = 3;
            this.label4.Text = "City";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(40, 350);
            this.label5.Margin = new System.Windows.Forms.Padding(20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 39);
            this.label5.TabIndex = 4;
            this.label5.Text = "Phone";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(40, 428);
            this.label6.Margin = new System.Windows.Forms.Padding(20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 39);
            this.label6.TabIndex = 5;
            this.label6.Text = "Fax";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(40, 506);
            this.label7.Margin = new System.Windows.Forms.Padding(20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 39);
            this.label7.TabIndex = 6;
            this.label7.Text = "Email";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(40, 584);
            this.label8.Margin = new System.Windows.Forms.Padding(20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(138, 39);
            this.label8.TabIndex = 7;
            this.label8.Text = "Latitude";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(40, 662);
            this.label9.Margin = new System.Windows.Forms.Padding(20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(167, 39);
            this.label9.TabIndex = 8;
            this.label9.Text = "Longitude";
            // 
            // new_office_name_txt
            // 
            this.new_office_name_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_office_name_txt.Location = new System.Drawing.Point(258, 40);
            this.new_office_name_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_office_name_txt.Name = "new_office_name_txt";
            this.new_office_name_txt.Size = new System.Drawing.Size(500, 41);
            this.new_office_name_txt.TabIndex = 9;
            // 
            // new_office_address_txt
            // 
            this.new_office_address_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_office_address_txt.Location = new System.Drawing.Point(258, 118);
            this.new_office_address_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_office_address_txt.Name = "new_office_address_txt";
            this.new_office_address_txt.Size = new System.Drawing.Size(500, 41);
            this.new_office_address_txt.TabIndex = 10;
            // 
            // new_office_zip_txt
            // 
            this.new_office_zip_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_office_zip_txt.Location = new System.Drawing.Point(258, 196);
            this.new_office_zip_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_office_zip_txt.Name = "new_office_zip_txt";
            this.new_office_zip_txt.Size = new System.Drawing.Size(500, 41);
            this.new_office_zip_txt.TabIndex = 11;
            // 
            // new_office_city_txt
            // 
            this.new_office_city_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_office_city_txt.Location = new System.Drawing.Point(258, 274);
            this.new_office_city_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_office_city_txt.Name = "new_office_city_txt";
            this.new_office_city_txt.Size = new System.Drawing.Size(500, 41);
            this.new_office_city_txt.TabIndex = 12;
            // 
            // new_office_phone_txt
            // 
            this.new_office_phone_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_office_phone_txt.Location = new System.Drawing.Point(258, 352);
            this.new_office_phone_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_office_phone_txt.Name = "new_office_phone_txt";
            this.new_office_phone_txt.Size = new System.Drawing.Size(500, 41);
            this.new_office_phone_txt.TabIndex = 13;
            // 
            // new_office_fax_txt
            // 
            this.new_office_fax_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_office_fax_txt.Location = new System.Drawing.Point(258, 430);
            this.new_office_fax_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_office_fax_txt.Name = "new_office_fax_txt";
            this.new_office_fax_txt.Size = new System.Drawing.Size(500, 41);
            this.new_office_fax_txt.TabIndex = 14;
            // 
            // new_office_email_txt
            // 
            this.new_office_email_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_office_email_txt.Location = new System.Drawing.Point(258, 508);
            this.new_office_email_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_office_email_txt.Name = "new_office_email_txt";
            this.new_office_email_txt.Size = new System.Drawing.Size(500, 41);
            this.new_office_email_txt.TabIndex = 15;
            // 
            // new_office_latitude_txt
            // 
            this.new_office_latitude_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_office_latitude_txt.Location = new System.Drawing.Point(258, 586);
            this.new_office_latitude_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_office_latitude_txt.Name = "new_office_latitude_txt";
            this.new_office_latitude_txt.Size = new System.Drawing.Size(500, 41);
            this.new_office_latitude_txt.TabIndex = 16;
            // 
            // new_office_longitude_txt
            // 
            this.new_office_longitude_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_office_longitude_txt.Location = new System.Drawing.Point(258, 664);
            this.new_office_longitude_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_office_longitude_txt.Name = "new_office_longitude_txt";
            this.new_office_longitude_txt.Size = new System.Drawing.Size(500, 41);
            this.new_office_longitude_txt.TabIndex = 17;
            // 
            // new_office_back_btn
            // 
            this.new_office_back_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_office_back_btn.Location = new System.Drawing.Point(70, 821);
            this.new_office_back_btn.Margin = new System.Windows.Forms.Padding(50, 100, 0, 20);
            this.new_office_back_btn.Name = "new_office_back_btn";
            this.new_office_back_btn.Size = new System.Drawing.Size(200, 60);
            this.new_office_back_btn.TabIndex = 18;
            this.new_office_back_btn.Text = "Back";
            this.new_office_back_btn.UseVisualStyleBackColor = true;
            this.new_office_back_btn.Click += new System.EventHandler(this.new_office_back_btn_Click);
            // 
            // new_office_save_btn
            // 
            this.new_office_save_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_office_save_btn.Location = new System.Drawing.Point(528, 822);
            this.new_office_save_btn.Margin = new System.Windows.Forms.Padding(50, 100, 0, 20);
            this.new_office_save_btn.Name = "new_office_save_btn";
            this.new_office_save_btn.Size = new System.Drawing.Size(200, 60);
            this.new_office_save_btn.TabIndex = 19;
            this.new_office_save_btn.Text = "Save";
            this.new_office_save_btn.UseVisualStyleBackColor = true;
            this.new_office_save_btn.Click += new System.EventHandler(this.new_office_save_btn_Click);
            // 
            // NewOffice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(240F, 240F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(809, 1074);
            this.Controls.Add(this.new_office_save_btn);
            this.Controls.Add(this.new_office_back_btn);
            this.Controls.Add(this.new_office_longitude_txt);
            this.Controls.Add(this.new_office_latitude_txt);
            this.Controls.Add(this.new_office_email_txt);
            this.Controls.Add(this.new_office_fax_txt);
            this.Controls.Add(this.new_office_phone_txt);
            this.Controls.Add(this.new_office_city_txt);
            this.Controls.Add(this.new_office_zip_txt);
            this.Controls.Add(this.new_office_address_txt);
            this.Controls.Add(this.new_office_name_txt);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "NewOffice";
            this.Padding = new System.Windows.Forms.Padding(20);
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "New Office";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox new_office_name_txt;
        private System.Windows.Forms.TextBox new_office_address_txt;
        private System.Windows.Forms.TextBox new_office_zip_txt;
        private System.Windows.Forms.TextBox new_office_city_txt;
        private System.Windows.Forms.TextBox new_office_phone_txt;
        private System.Windows.Forms.TextBox new_office_fax_txt;
        private System.Windows.Forms.TextBox new_office_email_txt;
        private System.Windows.Forms.TextBox new_office_latitude_txt;
        private System.Windows.Forms.TextBox new_office_longitude_txt;
        private System.Windows.Forms.Button new_office_back_btn;
        private System.Windows.Forms.Button new_office_save_btn;
    }
}