﻿namespace View
{
    partial class NewEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.employee_username_txt = new System.Windows.Forms.TextBox();
            this.employee_direkte_txt = new System.Windows.Forms.TextBox();
            this.employee_mobile_txt = new System.Windows.Forms.TextBox();
            this.employee_save_btn = new System.Windows.Forms.Button();
            this.employee_back_btn = new System.Windows.Forms.Button();
            this.employee_email_txt = new System.Windows.Forms.TextBox();
            this.employee_position_txt = new System.Windows.Forms.TextBox();
            this.employee_name_txt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.employee_office_combobox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.employee_admin_checkBox = new System.Windows.Forms.CheckBox();
            this.employee_password_txt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.employee_picturebox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.employee_picturebox)).BeginInit();
            this.SuspendLayout();
            // 
            // employee_username_txt
            // 
            this.employee_username_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee_username_txt.Location = new System.Drawing.Point(579, 605);
            this.employee_username_txt.Margin = new System.Windows.Forms.Padding(20);
            this.employee_username_txt.Name = "employee_username_txt";
            this.employee_username_txt.Size = new System.Drawing.Size(474, 41);
            this.employee_username_txt.TabIndex = 31;
            // 
            // employee_direkte_txt
            // 
            this.employee_direkte_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee_direkte_txt.Location = new System.Drawing.Point(579, 448);
            this.employee_direkte_txt.Margin = new System.Windows.Forms.Padding(20);
            this.employee_direkte_txt.Name = "employee_direkte_txt";
            this.employee_direkte_txt.Size = new System.Drawing.Size(474, 41);
            this.employee_direkte_txt.TabIndex = 30;
            // 
            // employee_mobile_txt
            // 
            this.employee_mobile_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee_mobile_txt.Location = new System.Drawing.Point(579, 367);
            this.employee_mobile_txt.Margin = new System.Windows.Forms.Padding(20);
            this.employee_mobile_txt.Name = "employee_mobile_txt";
            this.employee_mobile_txt.Size = new System.Drawing.Size(474, 41);
            this.employee_mobile_txt.TabIndex = 29;
            // 
            // employee_save_btn
            // 
            this.employee_save_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee_save_btn.Location = new System.Drawing.Point(823, 812);
            this.employee_save_btn.Margin = new System.Windows.Forms.Padding(0, 100, 50, 20);
            this.employee_save_btn.Name = "employee_save_btn";
            this.employee_save_btn.Size = new System.Drawing.Size(200, 60);
            this.employee_save_btn.TabIndex = 28;
            this.employee_save_btn.Text = "Save";
            this.employee_save_btn.UseVisualStyleBackColor = true;
            this.employee_save_btn.Click += new System.EventHandler(this.employee_save_btn_Click);
            // 
            // employee_back_btn
            // 
            this.employee_back_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee_back_btn.Location = new System.Drawing.Point(70, 812);
            this.employee_back_btn.Margin = new System.Windows.Forms.Padding(50, 100, 0, 20);
            this.employee_back_btn.Name = "employee_back_btn";
            this.employee_back_btn.Size = new System.Drawing.Size(200, 60);
            this.employee_back_btn.TabIndex = 27;
            this.employee_back_btn.Text = "Back";
            this.employee_back_btn.UseVisualStyleBackColor = true;
            this.employee_back_btn.Click += new System.EventHandler(this.employee_back_btn_Click);
            // 
            // employee_email_txt
            // 
            this.employee_email_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee_email_txt.Location = new System.Drawing.Point(579, 286);
            this.employee_email_txt.Margin = new System.Windows.Forms.Padding(20);
            this.employee_email_txt.Name = "employee_email_txt";
            this.employee_email_txt.Size = new System.Drawing.Size(474, 41);
            this.employee_email_txt.TabIndex = 26;
            // 
            // employee_position_txt
            // 
            this.employee_position_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee_position_txt.Location = new System.Drawing.Point(579, 121);
            this.employee_position_txt.Margin = new System.Windows.Forms.Padding(20);
            this.employee_position_txt.Name = "employee_position_txt";
            this.employee_position_txt.Size = new System.Drawing.Size(474, 41);
            this.employee_position_txt.TabIndex = 25;
            // 
            // employee_name_txt
            // 
            this.employee_name_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee_name_txt.Location = new System.Drawing.Point(579, 40);
            this.employee_name_txt.Margin = new System.Windows.Forms.Padding(20);
            this.employee_name_txt.Name = "employee_name_txt";
            this.employee_name_txt.Size = new System.Drawing.Size(474, 41);
            this.employee_name_txt.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(380, 606);
            this.label7.Margin = new System.Windows.Forms.Padding(20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(174, 39);
            this.label7.TabIndex = 23;
            this.label7.Text = "Username";
            // 
            // employee_office_combobox
            // 
            this.employee_office_combobox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee_office_combobox.FormattingEnabled = true;
            this.employee_office_combobox.Location = new System.Drawing.Point(579, 202);
            this.employee_office_combobox.Margin = new System.Windows.Forms.Padding(20);
            this.employee_office_combobox.Name = "employee_office_combobox";
            this.employee_office_combobox.Size = new System.Drawing.Size(474, 44);
            this.employee_office_combobox.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(380, 41);
            this.label6.Margin = new System.Windows.Forms.Padding(20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 39);
            this.label6.TabIndex = 21;
            this.label6.Text = "Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(380, 449);
            this.label5.Margin = new System.Windows.Forms.Padding(20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 39);
            this.label5.TabIndex = 20;
            this.label5.Text = "Direkte";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(380, 368);
            this.label4.Margin = new System.Windows.Forms.Padding(20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 39);
            this.label4.TabIndex = 19;
            this.label4.Text = "Mobile";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(380, 287);
            this.label3.Margin = new System.Windows.Forms.Padding(20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 39);
            this.label3.TabIndex = 18;
            this.label3.Text = "Email";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(380, 203);
            this.label2.Margin = new System.Windows.Forms.Padding(20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 39);
            this.label2.TabIndex = 17;
            this.label2.Text = "Office";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(380, 122);
            this.label1.Margin = new System.Windows.Forms.Padding(20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 39);
            this.label1.TabIndex = 16;
            this.label1.Text = "Position";
            // 
            // employee_admin_checkBox
            // 
            this.employee_admin_checkBox.AutoSize = true;
            this.employee_admin_checkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee_admin_checkBox.Location = new System.Drawing.Point(387, 529);
            this.employee_admin_checkBox.Margin = new System.Windows.Forms.Padding(20);
            this.employee_admin_checkBox.Name = "employee_admin_checkBox";
            this.employee_admin_checkBox.Size = new System.Drawing.Size(439, 40);
            this.employee_admin_checkBox.TabIndex = 32;
            this.employee_admin_checkBox.Text = "DTK Road App Administrator";
            this.employee_admin_checkBox.UseVisualStyleBackColor = true;
            this.employee_admin_checkBox.CheckedChanged += new System.EventHandler(this.employee_admin_checkBox_CheckedChanged);
            // 
            // employee_password_txt
            // 
            this.employee_password_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employee_password_txt.Location = new System.Drawing.Point(579, 687);
            this.employee_password_txt.Margin = new System.Windows.Forms.Padding(20);
            this.employee_password_txt.Name = "employee_password_txt";
            this.employee_password_txt.Size = new System.Drawing.Size(474, 41);
            this.employee_password_txt.TabIndex = 34;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(380, 688);
            this.label8.Margin = new System.Windows.Forms.Padding(20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(167, 39);
            this.label8.TabIndex = 33;
            this.label8.Text = "Password";
            // 
            // employee_picturebox
            // 
            this.employee_picturebox.Location = new System.Drawing.Point(40, 40);
            this.employee_picturebox.Margin = new System.Windows.Forms.Padding(20);
            this.employee_picturebox.Name = "employee_picturebox";
            this.employee_picturebox.Size = new System.Drawing.Size(300, 300);
            this.employee_picturebox.TabIndex = 35;
            this.employee_picturebox.TabStop = false;
            this.employee_picturebox.Click += new System.EventHandler(this.employee_picturebox_Click);
            // 
            // NewEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(240F, 240F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1093, 912);
            this.Controls.Add(this.employee_picturebox);
            this.Controls.Add(this.employee_password_txt);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.employee_admin_checkBox);
            this.Controls.Add(this.employee_username_txt);
            this.Controls.Add(this.employee_direkte_txt);
            this.Controls.Add(this.employee_mobile_txt);
            this.Controls.Add(this.employee_save_btn);
            this.Controls.Add(this.employee_back_btn);
            this.Controls.Add(this.employee_email_txt);
            this.Controls.Add(this.employee_position_txt);
            this.Controls.Add(this.employee_name_txt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.employee_office_combobox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "NewEmployee";
            this.Padding = new System.Windows.Forms.Padding(20);
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Employee";
            ((System.ComponentModel.ISupportInitialize)(this.employee_picturebox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox employee_username_txt;
        private System.Windows.Forms.TextBox employee_direkte_txt;
        private System.Windows.Forms.TextBox employee_mobile_txt;
        private System.Windows.Forms.Button employee_save_btn;
        private System.Windows.Forms.Button employee_back_btn;
        private System.Windows.Forms.TextBox employee_email_txt;
        private System.Windows.Forms.TextBox employee_position_txt;
        private System.Windows.Forms.TextBox employee_name_txt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox employee_office_combobox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox employee_admin_checkBox;
        private System.Windows.Forms.TextBox employee_password_txt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox employee_picturebox;
    }
}