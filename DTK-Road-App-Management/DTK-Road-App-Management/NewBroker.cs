﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
using Controller;

namespace View
{
    public partial class NewBroker : Form
    {

        private readonly HomeForm homeForm;
        private Broker broker;
        private IBrokerController controller;

        public NewBroker(HomeForm homeForm, Broker broker)
        {
            InitializeComponent();
            controller = new BrokerControllerImpl();
            this.homeForm = homeForm;
            this.broker = broker;

            if (broker != null)
                BrokerToText(broker);

        }

        private void new_broker_save_btn_Click(object sender, EventArgs e)
        {
            int error = 0;
            Broker temp = null;
            if (broker == null)
            {
                temp = BrokerFromText(false);
                error = controller.AddBroker(temp);
            }
            else
            {
                temp = BrokerFromText(true);
                error = controller.UpdateBroker(temp);
            }
            ErrorHandler(error);
        }

        private void new_broker_back_btn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ErrorHandler(int error)
        {
            string message = "";

            if (error == 0)
            {
                message = "There was an error adding a new Broker !";
                MessageBox.Show(message, "Broker", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (error == -1)
            {
                message = "Broker already exists!";
                MessageBox.Show(message, "Broker", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (error == -2)
            {
                message = "Username already exists!";
                MessageBox.Show(message, "Broker", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                message = "Broker Saved Successfully !";
                MessageBox.Show(message, "Broker", MessageBoxButtons.OK, MessageBoxIcon.Information);
                homeForm.UpdateBrokerDataGridView();
                ClearTextBoxes();
                Close();
            }
        }
        private void ClearTextBoxes()
        {
            new_broker_name_txt.Clear();
            new_broker_email_txt.Clear();
            new_broker_username_txt.Clear();
            new_broker_password_txt.Clear();
            new_broker_email_to_txt.Clear();
        }
        private void BrokerToText(Broker broker)
        {
            new_broker_name_txt.Text = broker.Name;
            new_broker_email_txt.Text = broker.Email;
            new_broker_username_txt.Text = broker.LoginUser.Username;
            new_broker_password_txt.Text = broker.LoginUser.Password;
            // new_broker_email_to_txt.Text = broker.send Email TO
        }
        private Broker BrokerFromText(bool hasId)
        {
            string name = new_broker_name_txt.Text;
            string email = new_broker_email_txt.Text;
            string username = new_broker_username_txt.Text;
            string password = new_broker_password_txt.Text;
            string emailTo = new_broker_email_to_txt.Text;

            LoginUser user = null;

            if (hasId)
            {
                user = new LoginUser(broker.LoginUser.Id, username, password, Rights.USER);
                return new Broker(broker.Id, name, email, user);
            }

            user = new LoginUser(username, password, Rights.USER);
            return new Broker(name, email, user);
        }
    }
}
