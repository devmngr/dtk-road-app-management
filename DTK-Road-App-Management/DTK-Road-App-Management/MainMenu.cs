﻿using System;
using System.Data;
using System.Windows.Forms;
using Controller;
using Model;
using Util;
namespace View
{
    public partial class HomeForm : Form
    {
        private IGasStationController gasStationController;
        private ITruckWashController truckWashController;
        private IOfficeController officeController;
        private IEmployeeController employeeController;
        private ILoginUserController loginUserController;
        private IBrokerController brokerController;
        private IHaulierController haulierController;
        private static char delimiter = ';';
        public HomeForm()
        {
            InitializeComponent();
            InitializeControllers();
            FillDataGridViews();
            home_tab_control.SelectedIndexChanged += new EventHandler(home_tab_control_SelectedIndexChanged);

        }
        private void home_csv_upload_btn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "CSV files (*.csv)|*.csv";
            openFileDialog.FilterIndex = 2;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;

                DataTable dt = gasStationController.DataTableFromCsv(filePath, delimiter);

                GasStationsFromCsv cs = new GasStationsFromCsv(this, dt, gasStationController);
                cs.ShowDialog();
            }
        }
        private void home_exit_btn_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
            Application.Exit();
        }
        private void home_delete_btn_Click(object sender, EventArgs e)
        {
            int error = 0;

            if (home_tab_control.SelectedTab == home_gas_stations_tab)
            {


            }
            else if (home_tab_control.SelectedTab == home_truck_wash_tab)
            {
                TruckWash temp = SelectedRowToTruckWash();
                DialogResult result = ShowDeletionDialog("Truck Wash", temp.ToString());
                if (result == DialogResult.Yes)
                {
                    error = truckWashController.DeleteTruckWash(temp);
                    ErrorHandler(error, "Truck Wash");
                    UpdateTruckWashDataGridView();
                }

            }
            else if (home_tab_control.SelectedTab == home_office_tab)
            {
                Office temp = SelectedRowToOffice();
                DialogResult result = ShowDeletionDialog("Office", temp.ToString());
                if (result == DialogResult.Yes)
                {
                    error = officeController.DeleteOffice(temp);
                    ErrorHandler(error, "Office");
                    UpdateOfficeDataGridView();
                }
            }
            else if (home_tab_control.SelectedTab == home_broker_tab)
            {
                Broker temp = SelectedRowToBroker();
                DialogResult result = ShowDeletionDialog(ViewColumnNames.broker_str, temp.ToString());
                if (result == DialogResult.Yes)
                {
                    error = brokerController.DeleteBroker(temp);
                    ErrorHandler(error, ViewColumnNames.broker_str);
                    UpdateBrokerDataGridView();
                    UpdateHaulierDataGridView();
                }

            }
            else if (home_tab_control.SelectedTab == home_haulier_tab)
            {
                Haulier temp = SelectedRowToHaulier();
                DialogResult result = ShowDeletionDialog("Haulier", temp.ToString());
                if (result == DialogResult.Yes)
                {
                    error = haulierController.DeleteHaulier(temp);
                    ErrorHandler(error, "Haulier");
                    UpdateHaulierDataGridView();
                }
            }
            else if (home_tab_control.SelectedTab == home_employee_tab)
            {
                Employee emp = SelectedRowToEmployee();
                DialogResult result = ShowDeletionDialog("Employee", emp.ToString());
                if (result == DialogResult.Yes)
                {
                    error = employeeController.DeleteEmployee(emp);
                    ErrorHandler(error, "Employee");
                    UpdateEmployeeDataGridView();
                }
            }


        }
        private void home_save_btn_Click(object sender, EventArgs e)
        {
            if (home_tab_control.SelectedTab == home_gas_stations_tab)
            {

            }

            else if (home_tab_control.SelectedTab == home_truck_wash_tab)
            {


            }
        }
        private void home_new_btn_Click(object sender, EventArgs e)
        {
            if (home_tab_control.SelectedTab == home_truck_wash_tab)
            {
                NewTruckWash truckWashForm = new NewTruckWash(this, null);
                truckWashForm.ShowDialog();
            }
            else if (home_tab_control.SelectedTab == home_office_tab)
            {
                NewOffice officeForm = new NewOffice(this, null);
                officeForm.ShowDialog();
            }
            else if (home_tab_control.SelectedTab == home_employee_tab)
            {
                NewEmployee employeeForm = new NewEmployee(this, null);
                employeeForm.ShowDialog();
            }
            else if (home_tab_control.SelectedTab == home_login_info_tab)
            {
                //OpenFrom<NewUser>();
            }
            else if (home_tab_control.SelectedTab == home_broker_tab)
            {
                NewBroker brokerForm = new NewBroker(this, null);
                brokerForm.ShowDialog();
            }
            else if (home_tab_control.SelectedTab == home_haulier_tab)
            {
                NewHaulier haulierForm = new NewHaulier(this, null);
                haulierForm.ShowDialog();
            }
        }
        private void home_edit_btn_Click(object sender, EventArgs e)
        {
            if (home_tab_control.SelectedTab == home_truck_wash_tab)
            {
                TruckWash temp = SelectedRowToTruckWash();
                NewTruckWash form = new NewTruckWash(this, temp);
                form.ShowDialog();
            }
            else if (home_tab_control.SelectedTab == home_office_tab)
            {
                Office temp = SelectedRowToOffice();
                NewOffice office = new NewOffice(this, temp);
                office.ShowDialog();
            }
            else if (home_tab_control.SelectedTab == home_employee_tab)
            {
                Employee emp = SelectedRowToEmployee();
                NewEmployee empForm = new NewEmployee(this, emp);
                empForm.ShowDialog();
            }
            else if (home_tab_control.SelectedTab == home_broker_tab)
            {
                Broker temp = SelectedRowToBroker();
                NewBroker broker = new NewBroker(this, temp);
                broker.ShowDialog();
            }
            else if (home_tab_control.SelectedTab == home_haulier_tab)
            {
                Haulier temp = SelectedRowToHaulier();
                NewHaulier haulier = new NewHaulier(this, temp);
                haulier.ShowDialog();
            }
        }
        private void InitializeControllers()
        {
            gasStationController = new GasStationControllerImpl();
            truckWashController = new TruckWashControllerImpl();
            officeController = new OfficeControllerImpl();
            employeeController = new EmployeeControllerImpl();
            loginUserController = new LoginUserControllerImpl();
            brokerController = new BrokerControllerImpl();
            haulierController = new HaulierControllerImpl();
        }
        private void FillDataGridViews()
        {
            UpdateGasStationDataGridView();
            UpdateTruckWashDataGridView();
            UpdateOfficeDataGridView();
            UpdateEmployeeDataGridView();
            UpdateLoginUserDataGridView();
            UpdateBrokerDataGridView();
            UpdateHaulierDataGridView();
        }
        private DataGridViewRow getSelectedRow(DataGridView dataGridView)
        {
            int index = dataGridView.SelectedRows[0].Index;
            DataGridViewRow selectedRow = dataGridView.Rows[index];
            return selectedRow;
        }
        private Office SelectedRowToOffice()
        {
            DataGridViewRow row = getSelectedRow(office_dataGridView);
            return RowToOffice(row);
        }
        private Office RowToOffice(DataGridViewRow row)
        {
            int id = Int32.Parse(row.Cells[ViewColumnNames.officeId_str].FormattedValue.ToString());
            string name = row.Cells[ViewColumnNames.name_str].FormattedValue.ToString();
            string address = row.Cells[ViewColumnNames.address_str].FormattedValue.ToString();
            string zipCode = row.Cells[ViewColumnNames.zip_str].FormattedValue.ToString();
            string city = row.Cells[ViewColumnNames.city_str].FormattedValue.ToString();
            string phoneNr = row.Cells[ViewColumnNames.phoneNr_str].FormattedValue.ToString();
            string faxNr = row.Cells[ViewColumnNames.faxNr_str].FormattedValue.ToString();
            string email = row.Cells[ViewColumnNames.email_str].FormattedValue.ToString();
            string latitude = row.Cells[ViewColumnNames.latitude_str].FormattedValue.ToString();
            string longitude = row.Cells[ViewColumnNames.longitude_str].FormattedValue.ToString();

            return new Office(id, name, address, zipCode, city, phoneNr, faxNr, email, latitude, longitude);

        }
        private LoginUser SelectedRowToLoginUser()
        {

            DataGridViewRow row = getSelectedRow(login_user_dataGridView);
            return RowToLoginUser(row);

        }
        private LoginUser RowToLoginUser(DataGridViewRow row)
        {
            int loginId = Int32.Parse(row.Cells[ViewColumnNames.loginId_str].FormattedValue.ToString());
            string username = row.Cells[ViewColumnNames.username_str].FormattedValue.ToString();
            string password = row.Cells[ViewColumnNames.password_str].FormattedValue.ToString();
            string rights = row.Cells[ViewColumnNames.rights_str].FormattedValue.ToString();

            if (rights.Equals(Utilities.GetEnumDescription(Rights.USER)))
                return new LoginUser(loginId, username, password, Rights.USER);

            else
                return new LoginUser(loginId, username, password, Rights.ADMIN);
        }
        private TruckWash SelectedRowToTruckWash()
        {
            DataGridViewRow row = getSelectedRow(truck_wash_dataGridView);
            return RowToTruckWash(row);

        }
        private TruckWash RowToTruckWash(DataGridViewRow row)
        {
            int id = Int32.Parse(row.Cells[ViewColumnNames.truckWashId_str].FormattedValue.ToString());
            string name = row.Cells[ViewColumnNames.name_str].FormattedValue.ToString();
            string address = row.Cells[ViewColumnNames.address_str].FormattedValue.ToString();
            string zip = row.Cells[ViewColumnNames.zip_str].FormattedValue.ToString();
            string city = row.Cells[ViewColumnNames.city_str].FormattedValue.ToString();
            string phone = row.Cells[ViewColumnNames.phoneNr_str].FormattedValue.ToString();
            string email = row.Cells[ViewColumnNames.email_str].FormattedValue.ToString();
            string website = row.Cells[ViewColumnNames.website_str].FormattedValue.ToString();
            string latitude = row.Cells[ViewColumnNames.latitude_str].FormattedValue.ToString();
            string longitude = row.Cells[ViewColumnNames.longitude_str].FormattedValue.ToString();
            string openingHours = row.Cells[ViewColumnNames.openingHours_str].FormattedValue.ToString();
            string remarks = row.Cells[ViewColumnNames.remarks_str].FormattedValue.ToString();
            string sTruck = row.Cells[ViewColumnNames.truckStandardPrice_str].FormattedValue.ToString();
            string sTrailer = row.Cells[ViewColumnNames.trailerStandardPrice_str].FormattedValue.ToString();
            string sTruckTrailer = row.Cells[ViewColumnNames.truckTrailerStandardPrice_str].FormattedValue.ToString();
            string qwTruck = row.Cells[ViewColumnNames.truckQuickWashPrice_str].FormattedValue.ToString();
            string qwTrailer = row.Cells[ViewColumnNames.trailerQuickWashPrice_str].FormattedValue.ToString();
            string qwTruckTrailer = row.Cells[ViewColumnNames.truckTrailerQuickWashPrice_str].FormattedValue.ToString();

            return new TruckWash(id, name, address, zip, city, phone, email, openingHours,
                sTruck, sTrailer, sTruckTrailer, qwTruck, qwTrailer, qwTruckTrailer,
                remarks, website, latitude, longitude);
        }
        private Broker SelectedRowToBroker()
        {
            DataGridViewRow row = getSelectedRow(broker_dataGridView);
            return RowToBroker(row, true);
        }
        private Broker RowToBroker(DataGridViewRow row, bool hasLogin)
        {
            int brokerId = Int32.Parse(row.Cells[ViewColumnNames.brokerId_str].FormattedValue.ToString());
            string name = row.Cells[ViewColumnNames.name_str].FormattedValue.ToString();
            string email = row.Cells[ViewColumnNames.email_str].FormattedValue.ToString();
            LoginUser user = RowToLoginUser(row);
            return new Broker(brokerId, name, email, user);
        }
        private Broker RowToBrokerForHaulier(DataGridViewRow row, bool hasLogin)
        {
            int brokerId = Int32.Parse(row.Cells[ViewColumnNames.brokerId_str].FormattedValue.ToString());
            string broker = row.Cells[ViewColumnNames.broker_str].FormattedValue.ToString();
            string email = row.Cells[ViewColumnNames.email_str].FormattedValue.ToString();
            return new Broker(brokerId, broker, email);
        }
        private Office RowToOfficeForEmployee(DataGridViewRow row, bool hasLogin)
        {
            int officeId = Int32.Parse(row.Cells[ViewColumnNames.officeId_str].FormattedValue.ToString());
            string officeName = row.Cells[ViewColumnNames.office_str].FormattedValue.ToString();
            return new Office(officeId, officeName);
        }
        private Haulier SelectedRowToHaulier()
        {
            DataGridViewRow row = getSelectedRow(haulier_dataGridView);
            LoginUser user = RowToLoginUser(row);
            Broker broker = RowToBrokerForHaulier(row, false);

            int haulierId = Int32.Parse(row.Cells[ViewColumnNames.haulierId_str].FormattedValue.ToString());
            string name = row.Cells[ViewColumnNames.name_str].FormattedValue.ToString();
            string email = row.Cells[ViewColumnNames.email_str].FormattedValue.ToString();
            string truckNr = row.Cells[ViewColumnNames.truckNr_str].FormattedValue.ToString();

            int brokerId = Int32.Parse(row.Cells[ViewColumnNames.brokerId_str].FormattedValue.ToString());
            string brokerName = row.Cells[ViewColumnNames.broker_str].FormattedValue.ToString();




            return new Haulier(haulierId, name, email, truckNr, user, broker);
        }
        private Employee SelectedRowToEmployee()
        {
            DataGridViewRow row = getSelectedRow(employee_dataGridView);
            return RowToEmployee(row, true);
        }
        private Employee RowToEmployee(DataGridViewRow row, bool hasLogin)
        {
            LoginUser user = RowToLoginUser(row);
            Office office = RowToOfficeForEmployee(row, false);
            int employeeId = Int32.Parse(row.Cells[ViewColumnNames.employeeId_str].FormattedValue.ToString());
            byte[] image = null;
            string name = row.Cells[ViewColumnNames.name_str].FormattedValue.ToString();
            string position = row.Cells[ViewColumnNames.position_str].FormattedValue.ToString();
            string email = row.Cells[ViewColumnNames.email_str].FormattedValue.ToString();
            string mobile = row.Cells[ViewColumnNames.mobileNr_str].FormattedValue.ToString();
            string direkte = row.Cells[ViewColumnNames.direkteNr_str].FormattedValue.ToString();

            return new Employee(employeeId, image, name, position, email, mobile, direkte, office, user);
        }
        private DialogResult ShowDeletionDialog(string objectName, string objectDetails)
        {
            string title = "Delete " + objectName;
            string message = "Are you sure you want to delete this " + objectName + "?\n\n" + objectDetails;
            return MessageBox.Show(message, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }
        private void ErrorHandler(int error, string objectName)
        {
            string message = "";

            if (error == 0)
            {
                message = "There was an error deleting" + objectName + "!";
                MessageBox.Show(message, "Office", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                message = objectName + " Deleted Successfully !";
                MessageBox.Show(message, objectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void home_tab_control_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (home_tab_control.SelectedIndex == 0)
                home_csv_upload_btn.Visible = true;
            else
                home_csv_upload_btn.Visible = false;
        }
        public void UpdateGasStationDataGridView() => gasStationController.FillGasStationGrid(gas_station_dataGridView);
        public void UpdateOfficeDataGridView() => officeController.FillOfficeGrid(office_dataGridView);
        public void UpdateTruckWashDataGridView() => truckWashController.FillTruckWashGrid(truck_wash_dataGridView);
        public void UpdateEmployeeDataGridView() => employeeController.FillEmployeeGrid(employee_dataGridView);
        public void UpdateLoginUserDataGridView() => loginUserController.FillLoginUserGrid(login_user_dataGridView);
        public void UpdateBrokerDataGridView() => brokerController.FillBrokerGrid(broker_dataGridView);
        public void UpdateHaulierDataGridView() => haulierController.FillHaulierGrid(haulier_dataGridView);
    }
}
