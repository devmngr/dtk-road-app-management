﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controller;
using Model;
namespace View
{
    public partial class NewTruckWash : Form
    {
        private ITruckWashController truckWashController;
        private readonly HomeForm homeForm;
        private TruckWash truckWash;

        public NewTruckWash(HomeForm homeForm, TruckWash truckWash)
        {
            InitializeComponent();
            truckWashController = new TruckWashControllerImpl();
            this.homeForm = homeForm;
            this.truckWash = truckWash;

            if (truckWash != null)
                TruckWashToText(truckWash);
        }

        private void new_truck_wash_save_btn_Click(object sender, EventArgs e)
        {
            int error = 0;
            if (truckWash == null)
            {
                truckWash = TruckWashFromText(false);
                error = truckWashController.AddNewTruckWash(truckWash);
            }
            else
            {
                truckWash = TruckWashFromText(true);
                error = truckWashController.UpdateTruckWash(truckWash);
            }

            ErrorHandler(error);

        }

        private void new_truck_wash_back_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ErrorHandler(int error)
        {
            string message = "";

            if (error == 0)
            {
                message = "There was an error adding a new Truck Wash !";
                MessageBox.Show(message, "Truck Wash", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                message = "Truck Wash Saved Successfully !";
                MessageBox.Show(message, "Truck Wash", MessageBoxButtons.OK, MessageBoxIcon.Information);
                homeForm.UpdateTruckWashDataGridView();
                ClearTextBoxes();
                Close();
            }
        }



        private void ClearTextBoxes()
        {
            new_truck_wash_name_txt.Clear();
            new_truck_wash_address_txt.Clear();
            new_truck_wash_zip_txt.Clear();
            new_truck_wash_city_txt.Clear();
            new_truck_wash_phone_txt.Clear();
            new_truck_wash_email_txt.Clear();
            new_truck_wash_openingH_txt.Clear();
            new_truck_wash_sTruck_txt.Clear();
            new_truck_wash_sTrailer_txt.Clear();
            new_truck_wash_sTruckTrailer_txt.Clear();
            new_truck_wash_qwTruck_txt.Clear();
            new_truck_wash_qwTrailer_txt.Clear();
            new_truck_wash_qwTruckTrailer_txt.Clear();
            new_truck_wash_remarks_txt.Clear();
            new_truck_wash_website_txt.Clear();
            new_truck_wash_latitude_txt.Clear();
            new_truck_wash_longitude_txt.Clear();
        }
        private TruckWash TruckWashFromText(bool hasId)
        {
            string name = new_truck_wash_name_txt.Text;
            string address = new_truck_wash_address_txt.Text;
            string zip = new_truck_wash_zip_txt.Text;
            string city = new_truck_wash_city_txt.Text;
            string phone = new_truck_wash_phone_txt.Text;
            string email = new_truck_wash_email_txt.Text;
            string openinghours = new_truck_wash_openingH_txt.Text;
            string truckStandardPrice = new_truck_wash_sTruck_txt.Text;
            string trailerStandardPrice = new_truck_wash_sTrailer_txt.Text;
            string truckTrailerStandardPrice = new_truck_wash_sTruckTrailer_txt.Text;
            string truckQouckWashPrice = new_truck_wash_qwTruck_txt.Text;
            string trailerQuickWashPrice = new_truck_wash_qwTrailer_txt.Text;
            string truckTrailerQuickWashPrice = new_truck_wash_qwTruckTrailer_txt.Text;
            string remarks = new_truck_wash_remarks_txt.Text;
            string website = new_truck_wash_website_txt.Text;
            string latitude = new_truck_wash_latitude_txt.Text;
            string longitude = new_truck_wash_longitude_txt.Text;


            if (hasId)
                return new TruckWash(truckWash.Id, name, address, zip, city, phone, email, openinghours,
                    truckStandardPrice, trailerStandardPrice, truckTrailerStandardPrice,
                    truckQouckWashPrice, trailerQuickWashPrice, truckTrailerQuickWashPrice,
                   remarks, website, latitude, longitude);

            else
                return new TruckWash(name, address, zip, city, phone, email, openinghours,
                        truckStandardPrice, trailerStandardPrice, truckTrailerStandardPrice,
                        truckQouckWashPrice, trailerQuickWashPrice, truckTrailerQuickWashPrice,
                       remarks, website, latitude, longitude);
        }
        private void TruckWashToText(TruckWash truckWash)
        {
            new_truck_wash_name_txt.Text = truckWash.Name;
            new_truck_wash_address_txt.Text = truckWash.Address;
            new_truck_wash_zip_txt.Text = truckWash.Zip;
            new_truck_wash_city_txt.Text = truckWash.City;
            new_truck_wash_phone_txt.Text = truckWash.Phone;
            new_truck_wash_email_txt.Text = truckWash.Email;
            new_truck_wash_website_txt.Text = truckWash.Website;
            new_truck_wash_latitude_txt.Text = truckWash.Latitude;
            new_truck_wash_longitude_txt.Text = truckWash.Longitude;
            new_truck_wash_openingH_txt.Text = truckWash.OpeningHours;
            new_truck_wash_remarks_txt.Text = truckWash.Remarks;
            new_truck_wash_sTruck_txt.Text = truckWash.TruckStandardPrice;
            new_truck_wash_qwTruck_txt.Text = truckWash.TruckQuickWashPrice;
            new_truck_wash_sTrailer_txt.Text = truckWash.TrailerStandardPrice;
            new_truck_wash_qwTrailer_txt.Text = truckWash.TrailerQuickWashPrice;
            new_truck_wash_sTruckTrailer_txt.Text = truckWash.TruckTrailerStandardPrice;
            new_truck_wash_qwTruckTrailer_txt.Text = truckWash.TruckTrailerQuickWashPrice;

        }
    }
}
