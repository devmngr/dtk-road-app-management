﻿namespace View
{
    partial class NewUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.new_user_type_comboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.new_user_first_name_txt = new System.Windows.Forms.TextBox();
            this.new_user_company_txt = new System.Windows.Forms.TextBox();
            this.new_user_last_name_txt = new System.Windows.Forms.TextBox();
            this.new_user_back_btn = new System.Windows.Forms.Button();
            this.new_user_save_btn = new System.Windows.Forms.Button();
            this.new_user_username_txt = new System.Windows.Forms.TextBox();
            this.new_user_password_txt = new System.Windows.Forms.TextBox();
            this.new_user_retype_password_txt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 117);
            this.label1.Margin = new System.Windows.Forms.Padding(20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Company";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(40, 195);
            this.label2.Margin = new System.Windows.Forms.Padding(20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 39);
            this.label2.TabIndex = 1;
            this.label2.Text = "First name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(40, 273);
            this.label3.Margin = new System.Windows.Forms.Padding(20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 39);
            this.label3.TabIndex = 2;
            this.label3.Text = "Last name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(40, 351);
            this.label4.Margin = new System.Windows.Forms.Padding(20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 39);
            this.label4.TabIndex = 3;
            this.label4.Text = "Username";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(40, 429);
            this.label5.Margin = new System.Windows.Forms.Padding(20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 39);
            this.label5.TabIndex = 4;
            this.label5.Text = "Password";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(40, 38);
            this.label6.Margin = new System.Windows.Forms.Padding(20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 39);
            this.label6.TabIndex = 5;
            this.label6.Text = "Type";
            // 
            // new_user_type_comboBox
            // 
            this.new_user_type_comboBox.FormattingEnabled = true;
            this.new_user_type_comboBox.Location = new System.Drawing.Point(371, 40);
            this.new_user_type_comboBox.Margin = new System.Windows.Forms.Padding(20);
            this.new_user_type_comboBox.Name = "new_user_type_comboBox";
            this.new_user_type_comboBox.Size = new System.Drawing.Size(392, 39);
            this.new_user_type_comboBox.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(40, 507);
            this.label7.Margin = new System.Windows.Forms.Padding(20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(291, 39);
            this.label7.TabIndex = 7;
            this.label7.Text = "Re-type password";
            // 
            // new_user_first_name_txt
            // 
            this.new_user_first_name_txt.Location = new System.Drawing.Point(371, 197);
            this.new_user_first_name_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_user_first_name_txt.Name = "new_user_first_name_txt";
            this.new_user_first_name_txt.Size = new System.Drawing.Size(392, 38);
            this.new_user_first_name_txt.TabIndex = 8;
            // 
            // new_user_company_txt
            // 
            this.new_user_company_txt.Location = new System.Drawing.Point(371, 119);
            this.new_user_company_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_user_company_txt.Name = "new_user_company_txt";
            this.new_user_company_txt.Size = new System.Drawing.Size(392, 38);
            this.new_user_company_txt.TabIndex = 9;
            // 
            // new_user_last_name_txt
            // 
            this.new_user_last_name_txt.Location = new System.Drawing.Point(371, 275);
            this.new_user_last_name_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_user_last_name_txt.Name = "new_user_last_name_txt";
            this.new_user_last_name_txt.Size = new System.Drawing.Size(392, 38);
            this.new_user_last_name_txt.TabIndex = 10;
            // 
            // new_user_back_btn
            // 
            this.new_user_back_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_user_back_btn.Location = new System.Drawing.Point(70, 666);
            this.new_user_back_btn.Margin = new System.Windows.Forms.Padding(50, 100, 0, 20);
            this.new_user_back_btn.Name = "new_user_back_btn";
            this.new_user_back_btn.Size = new System.Drawing.Size(200, 60);
            this.new_user_back_btn.TabIndex = 11;
            this.new_user_back_btn.Text = "Back";
            this.new_user_back_btn.UseVisualStyleBackColor = true;
            // 
            // new_user_save_btn
            // 
            this.new_user_save_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_user_save_btn.Location = new System.Drawing.Point(698, 666);
            this.new_user_save_btn.Margin = new System.Windows.Forms.Padding(0, 100, 50, 20);
            this.new_user_save_btn.Name = "new_user_save_btn";
            this.new_user_save_btn.Size = new System.Drawing.Size(200, 60);
            this.new_user_save_btn.TabIndex = 12;
            this.new_user_save_btn.Text = "Save";
            this.new_user_save_btn.UseVisualStyleBackColor = true;
            this.new_user_save_btn.Click += new System.EventHandler(this.new_user_save_btn_Click);
            // 
            // new_user_username_txt
            // 
            this.new_user_username_txt.Location = new System.Drawing.Point(371, 353);
            this.new_user_username_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_user_username_txt.Name = "new_user_username_txt";
            this.new_user_username_txt.Size = new System.Drawing.Size(392, 38);
            this.new_user_username_txt.TabIndex = 13;
            // 
            // new_user_password_txt
            // 
            this.new_user_password_txt.Location = new System.Drawing.Point(371, 431);
            this.new_user_password_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_user_password_txt.Name = "new_user_password_txt";
            this.new_user_password_txt.Size = new System.Drawing.Size(392, 38);
            this.new_user_password_txt.TabIndex = 14;
            // 
            // new_user_retype_password_txt
            // 
            this.new_user_retype_password_txt.Location = new System.Drawing.Point(371, 509);
            this.new_user_retype_password_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_user_retype_password_txt.Name = "new_user_retype_password_txt";
            this.new_user_retype_password_txt.Size = new System.Drawing.Size(392, 38);
            this.new_user_retype_password_txt.TabIndex = 15;
            // 
            // NewUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(240F, 240F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(968, 806);
            this.Controls.Add(this.new_user_retype_password_txt);
            this.Controls.Add(this.new_user_password_txt);
            this.Controls.Add(this.new_user_username_txt);
            this.Controls.Add(this.new_user_save_btn);
            this.Controls.Add(this.new_user_back_btn);
            this.Controls.Add(this.new_user_last_name_txt);
            this.Controls.Add(this.new_user_company_txt);
            this.Controls.Add(this.new_user_first_name_txt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.new_user_type_comboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "NewUser";
            this.Padding = new System.Windows.Forms.Padding(20);
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "User";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox new_user_type_comboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox new_user_first_name_txt;
        private System.Windows.Forms.TextBox new_user_company_txt;
        private System.Windows.Forms.TextBox new_user_last_name_txt;
        private System.Windows.Forms.Button new_user_back_btn;
        private System.Windows.Forms.Button new_user_save_btn;
        private System.Windows.Forms.TextBox new_user_username_txt;
        private System.Windows.Forms.TextBox new_user_password_txt;
        private System.Windows.Forms.TextBox new_user_retype_password_txt;
    }
}