﻿namespace View
{
    partial class NewTruckWash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.new_truck_wash_name_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_address_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_zip_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_city_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_phone_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_email_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_website_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_latitude_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_longitude_txt = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.new_truck_wash_openingH_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_remarks_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_sTruck_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_qwTruck_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_sTrailer_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_qwTrailer_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_sTruckTrailer_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_qwTruckTrailer_txt = new System.Windows.Forms.TextBox();
            this.new_truck_wash_back_btn = new System.Windows.Forms.Button();
            this.new_truck_wash_save_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(40, 113);
            this.label2.Margin = new System.Windows.Forms.Padding(20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 39);
            this.label2.TabIndex = 1;
            this.label2.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(40, 191);
            this.label3.Margin = new System.Windows.Forms.Padding(20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 39);
            this.label3.TabIndex = 2;
            this.label3.Text = "Zip";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(40, 269);
            this.label4.Margin = new System.Windows.Forms.Padding(20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 39);
            this.label4.TabIndex = 3;
            this.label4.Text = "City";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(40, 347);
            this.label5.Margin = new System.Windows.Forms.Padding(20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 39);
            this.label5.TabIndex = 4;
            this.label5.Text = "Phone";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(40, 425);
            this.label6.Margin = new System.Windows.Forms.Padding(20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 39);
            this.label6.TabIndex = 5;
            this.label6.Text = "Email";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(830, 40);
            this.label7.Margin = new System.Windows.Forms.Padding(20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(246, 39);
            this.label7.TabIndex = 6;
            this.label7.Text = "Opening Hours";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(923, 334);
            this.label8.Margin = new System.Windows.Forms.Padding(20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 39);
            this.label8.TabIndex = 7;
            this.label8.Text = "Remarks";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(40, 503);
            this.label9.Margin = new System.Windows.Forms.Padding(20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(140, 39);
            this.label9.TabIndex = 8;
            this.label9.Text = "Website";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(40, 659);
            this.label10.Margin = new System.Windows.Forms.Padding(20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(167, 39);
            this.label10.TabIndex = 9;
            this.label10.Text = "Longitude";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(40, 581);
            this.label11.Margin = new System.Windows.Forms.Padding(20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(138, 39);
            this.label11.TabIndex = 10;
            this.label11.Text = "Latitude";
            // 
            // new_truck_wash_name_txt
            // 
            this.new_truck_wash_name_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_name_txt.Location = new System.Drawing.Point(232, 36);
            this.new_truck_wash_name_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_name_txt.Name = "new_truck_wash_name_txt";
            this.new_truck_wash_name_txt.Size = new System.Drawing.Size(520, 41);
            this.new_truck_wash_name_txt.TabIndex = 11;
            // 
            // new_truck_wash_address_txt
            // 
            this.new_truck_wash_address_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_address_txt.Location = new System.Drawing.Point(232, 115);
            this.new_truck_wash_address_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_address_txt.Name = "new_truck_wash_address_txt";
            this.new_truck_wash_address_txt.Size = new System.Drawing.Size(520, 41);
            this.new_truck_wash_address_txt.TabIndex = 12;
            // 
            // new_truck_wash_zip_txt
            // 
            this.new_truck_wash_zip_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_zip_txt.Location = new System.Drawing.Point(232, 193);
            this.new_truck_wash_zip_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_zip_txt.Name = "new_truck_wash_zip_txt";
            this.new_truck_wash_zip_txt.Size = new System.Drawing.Size(520, 41);
            this.new_truck_wash_zip_txt.TabIndex = 13;
            // 
            // new_truck_wash_city_txt
            // 
            this.new_truck_wash_city_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_city_txt.Location = new System.Drawing.Point(232, 271);
            this.new_truck_wash_city_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_city_txt.Name = "new_truck_wash_city_txt";
            this.new_truck_wash_city_txt.Size = new System.Drawing.Size(520, 41);
            this.new_truck_wash_city_txt.TabIndex = 14;
            // 
            // new_truck_wash_phone_txt
            // 
            this.new_truck_wash_phone_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_phone_txt.Location = new System.Drawing.Point(232, 349);
            this.new_truck_wash_phone_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_phone_txt.Name = "new_truck_wash_phone_txt";
            this.new_truck_wash_phone_txt.Size = new System.Drawing.Size(520, 41);
            this.new_truck_wash_phone_txt.TabIndex = 15;
            // 
            // new_truck_wash_email_txt
            // 
            this.new_truck_wash_email_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_email_txt.Location = new System.Drawing.Point(232, 427);
            this.new_truck_wash_email_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_email_txt.Name = "new_truck_wash_email_txt";
            this.new_truck_wash_email_txt.Size = new System.Drawing.Size(520, 41);
            this.new_truck_wash_email_txt.TabIndex = 16;
            // 
            // new_truck_wash_website_txt
            // 
            this.new_truck_wash_website_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_website_txt.Location = new System.Drawing.Point(232, 505);
            this.new_truck_wash_website_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_website_txt.Name = "new_truck_wash_website_txt";
            this.new_truck_wash_website_txt.Size = new System.Drawing.Size(520, 41);
            this.new_truck_wash_website_txt.TabIndex = 17;
            // 
            // new_truck_wash_latitude_txt
            // 
            this.new_truck_wash_latitude_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_latitude_txt.Location = new System.Drawing.Point(232, 583);
            this.new_truck_wash_latitude_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_latitude_txt.Name = "new_truck_wash_latitude_txt";
            this.new_truck_wash_latitude_txt.Size = new System.Drawing.Size(520, 41);
            this.new_truck_wash_latitude_txt.TabIndex = 18;
            // 
            // new_truck_wash_longitude_txt
            // 
            this.new_truck_wash_longitude_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_longitude_txt.Location = new System.Drawing.Point(232, 661);
            this.new_truck_wash_longitude_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_longitude_txt.Name = "new_truck_wash_longitude_txt";
            this.new_truck_wash_longitude_txt.Size = new System.Drawing.Size(520, 41);
            this.new_truck_wash_longitude_txt.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(40, 787);
            this.label12.Margin = new System.Windows.Forms.Padding(20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(256, 39);
            this.label12.TabIndex = 22;
            this.label12.Text = "Standard Price";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(209, 865);
            this.label14.Margin = new System.Windows.Forms.Padding(20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 39);
            this.label14.TabIndex = 24;
            this.label14.Text = "Truck";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(197, 938);
            this.label15.Margin = new System.Windows.Forms.Padding(20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(114, 39);
            this.label15.TabIndex = 25;
            this.label15.Text = "Trailer";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(962, 938);
            this.label16.Margin = new System.Windows.Forms.Padding(20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(114, 39);
            this.label16.TabIndex = 30;
            this.label16.Text = "Trailer";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(974, 860);
            this.label17.Margin = new System.Windows.Forms.Padding(20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(102, 39);
            this.label17.TabIndex = 29;
            this.label17.Text = "Truck";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(844, 787);
            this.label18.Margin = new System.Windows.Forms.Padding(20);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(293, 39);
            this.label18.TabIndex = 28;
            this.label18.Text = "QuickWash Price";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(74, 1016);
            this.label13.Margin = new System.Windows.Forms.Padding(20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(237, 39);
            this.label13.TabIndex = 36;
            this.label13.Text = "Truck + Trailer";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(839, 1016);
            this.label19.Margin = new System.Windows.Forms.Padding(20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(237, 39);
            this.label19.TabIndex = 38;
            this.label19.Text = "Truck + Trailer";
            // 
            // new_truck_wash_openingH_txt
            // 
            this.new_truck_wash_openingH_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_openingH_txt.Location = new System.Drawing.Point(1116, 40);
            this.new_truck_wash_openingH_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_openingH_txt.Multiline = true;
            this.new_truck_wash_openingH_txt.Name = "new_truck_wash_openingH_txt";
            this.new_truck_wash_openingH_txt.Size = new System.Drawing.Size(412, 254);
            this.new_truck_wash_openingH_txt.TabIndex = 42;
            // 
            // new_truck_wash_remarks_txt
            // 
            this.new_truck_wash_remarks_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_remarks_txt.Location = new System.Drawing.Point(1116, 334);
            this.new_truck_wash_remarks_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_remarks_txt.Multiline = true;
            this.new_truck_wash_remarks_txt.Name = "new_truck_wash_remarks_txt";
            this.new_truck_wash_remarks_txt.Size = new System.Drawing.Size(412, 285);
            this.new_truck_wash_remarks_txt.TabIndex = 43;
            // 
            // new_truck_wash_sTruck_txt
            // 
            this.new_truck_wash_sTruck_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_sTruck_txt.Location = new System.Drawing.Point(351, 862);
            this.new_truck_wash_sTruck_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_sTruck_txt.Name = "new_truck_wash_sTruck_txt";
            this.new_truck_wash_sTruck_txt.Size = new System.Drawing.Size(307, 41);
            this.new_truck_wash_sTruck_txt.TabIndex = 44;
            // 
            // new_truck_wash_qwTruck_txt
            // 
            this.new_truck_wash_qwTruck_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_qwTruck_txt.Location = new System.Drawing.Point(1116, 862);
            this.new_truck_wash_qwTruck_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_qwTruck_txt.Name = "new_truck_wash_qwTruck_txt";
            this.new_truck_wash_qwTruck_txt.Size = new System.Drawing.Size(307, 41);
            this.new_truck_wash_qwTruck_txt.TabIndex = 45;
            // 
            // new_truck_wash_sTrailer_txt
            // 
            this.new_truck_wash_sTrailer_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_sTrailer_txt.Location = new System.Drawing.Point(351, 940);
            this.new_truck_wash_sTrailer_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_sTrailer_txt.Name = "new_truck_wash_sTrailer_txt";
            this.new_truck_wash_sTrailer_txt.Size = new System.Drawing.Size(307, 41);
            this.new_truck_wash_sTrailer_txt.TabIndex = 46;
            // 
            // new_truck_wash_qwTrailer_txt
            // 
            this.new_truck_wash_qwTrailer_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_qwTrailer_txt.Location = new System.Drawing.Point(1116, 940);
            this.new_truck_wash_qwTrailer_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_qwTrailer_txt.Name = "new_truck_wash_qwTrailer_txt";
            this.new_truck_wash_qwTrailer_txt.Size = new System.Drawing.Size(307, 41);
            this.new_truck_wash_qwTrailer_txt.TabIndex = 47;
            // 
            // new_truck_wash_sTruckTrailer_txt
            // 
            this.new_truck_wash_sTruckTrailer_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_sTruckTrailer_txt.Location = new System.Drawing.Point(351, 1018);
            this.new_truck_wash_sTruckTrailer_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_sTruckTrailer_txt.Name = "new_truck_wash_sTruckTrailer_txt";
            this.new_truck_wash_sTruckTrailer_txt.Size = new System.Drawing.Size(307, 41);
            this.new_truck_wash_sTruckTrailer_txt.TabIndex = 48;
            // 
            // new_truck_wash_qwTruckTrailer_txt
            // 
            this.new_truck_wash_qwTruckTrailer_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_qwTruckTrailer_txt.Location = new System.Drawing.Point(1116, 1018);
            this.new_truck_wash_qwTruckTrailer_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_truck_wash_qwTruckTrailer_txt.Name = "new_truck_wash_qwTruckTrailer_txt";
            this.new_truck_wash_qwTruckTrailer_txt.Size = new System.Drawing.Size(307, 41);
            this.new_truck_wash_qwTruckTrailer_txt.TabIndex = 49;
            // 
            // new_truck_wash_back_btn
            // 
            this.new_truck_wash_back_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_back_btn.Location = new System.Drawing.Point(70, 1175);
            this.new_truck_wash_back_btn.Margin = new System.Windows.Forms.Padding(50, 100, 0, 20);
            this.new_truck_wash_back_btn.Name = "new_truck_wash_back_btn";
            this.new_truck_wash_back_btn.Size = new System.Drawing.Size(200, 60);
            this.new_truck_wash_back_btn.TabIndex = 50;
            this.new_truck_wash_back_btn.Text = "Back";
            this.new_truck_wash_back_btn.UseVisualStyleBackColor = true;
            this.new_truck_wash_back_btn.Click += new System.EventHandler(this.new_truck_wash_back_btn_Click);
            // 
            // new_truck_wash_save_btn
            // 
            this.new_truck_wash_save_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_truck_wash_save_btn.Location = new System.Drawing.Point(1298, 1176);
            this.new_truck_wash_save_btn.Margin = new System.Windows.Forms.Padding(0, 100, 50, 20);
            this.new_truck_wash_save_btn.Name = "new_truck_wash_save_btn";
            this.new_truck_wash_save_btn.Size = new System.Drawing.Size(200, 60);
            this.new_truck_wash_save_btn.TabIndex = 51;
            this.new_truck_wash_save_btn.Text = "Save";
            this.new_truck_wash_save_btn.UseVisualStyleBackColor = true;
            this.new_truck_wash_save_btn.Click += new System.EventHandler(this.new_truck_wash_save_btn_Click);
            // 
            // NewTruckWash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(240F, 240F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1568, 1276);
            this.Controls.Add(this.new_truck_wash_save_btn);
            this.Controls.Add(this.new_truck_wash_back_btn);
            this.Controls.Add(this.new_truck_wash_qwTruckTrailer_txt);
            this.Controls.Add(this.new_truck_wash_sTruckTrailer_txt);
            this.Controls.Add(this.new_truck_wash_qwTrailer_txt);
            this.Controls.Add(this.new_truck_wash_sTrailer_txt);
            this.Controls.Add(this.new_truck_wash_qwTruck_txt);
            this.Controls.Add(this.new_truck_wash_sTruck_txt);
            this.Controls.Add(this.new_truck_wash_remarks_txt);
            this.Controls.Add(this.new_truck_wash_openingH_txt);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.new_truck_wash_longitude_txt);
            this.Controls.Add(this.new_truck_wash_latitude_txt);
            this.Controls.Add(this.new_truck_wash_website_txt);
            this.Controls.Add(this.new_truck_wash_email_txt);
            this.Controls.Add(this.new_truck_wash_phone_txt);
            this.Controls.Add(this.new_truck_wash_city_txt);
            this.Controls.Add(this.new_truck_wash_zip_txt);
            this.Controls.Add(this.new_truck_wash_address_txt);
            this.Controls.Add(this.new_truck_wash_name_txt);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "NewTruckWash";
            this.Padding = new System.Windows.Forms.Padding(20);
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "New Truck kWash";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox new_truck_wash_name_txt;
        private System.Windows.Forms.TextBox new_truck_wash_address_txt;
        private System.Windows.Forms.TextBox new_truck_wash_zip_txt;
        private System.Windows.Forms.TextBox new_truck_wash_city_txt;
        private System.Windows.Forms.TextBox new_truck_wash_phone_txt;
        private System.Windows.Forms.TextBox new_truck_wash_email_txt;
        private System.Windows.Forms.TextBox new_truck_wash_website_txt;
        private System.Windows.Forms.TextBox new_truck_wash_latitude_txt;
        private System.Windows.Forms.TextBox new_truck_wash_longitude_txt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox new_truck_wash_openingH_txt;
        private System.Windows.Forms.TextBox new_truck_wash_remarks_txt;
        private System.Windows.Forms.TextBox new_truck_wash_sTruck_txt;
        private System.Windows.Forms.TextBox new_truck_wash_qwTruck_txt;
        private System.Windows.Forms.TextBox new_truck_wash_sTrailer_txt;
        private System.Windows.Forms.TextBox new_truck_wash_qwTrailer_txt;
        private System.Windows.Forms.TextBox new_truck_wash_sTruckTrailer_txt;
        private System.Windows.Forms.TextBox new_truck_wash_qwTruckTrailer_txt;
        private System.Windows.Forms.Button new_truck_wash_back_btn;
        private System.Windows.Forms.Button new_truck_wash_save_btn;
    }
}