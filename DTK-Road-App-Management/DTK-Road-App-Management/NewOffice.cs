﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace View
{
    public partial class NewOffice : Form
    {
        private readonly HomeForm homeForm;
        private Office office;
        private IOfficeController controller;

        public NewOffice(HomeForm homeForm, Office office)
        {
            InitializeComponent();
            controller = new OfficeControllerImpl();
            this.homeForm = homeForm;
            this.office = office;

            if (office != null)
                OfficeToText(office);
        }

        private void new_office_save_btn_Click(object sender, EventArgs e)
        {
            int error = 0;
            if (office == null)
            {
                office = OfficeFromText(false);
                error = controller.AddOffice(office);
            }
            else
            {
                office = OfficeFromText(true);
                error = controller.UpdateOffice(office);
            }
            ErrorHandler(error);
        }

        private void new_office_back_btn_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void ErrorHandler(int error)
        {
            string message = "";

            if (error == 0)
            {
                message = "There was an error adding a new Office !";
                MessageBox.Show(message, "Office", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                message = "Office Saved Successfully !";
                MessageBox.Show(message, "Office", MessageBoxButtons.OK, MessageBoxIcon.Information);
                homeForm.UpdateOfficeDataGridView();
                ClearTextBoxes();
                Close();
            }
        }

        private void ClearTextBoxes()
        {
            new_office_name_txt.Clear();
            new_office_address_txt.Clear();
            new_office_zip_txt.Clear();
            new_office_city_txt.Clear();
            new_office_phone_txt.Clear();
            new_office_fax_txt.Clear();
            new_office_email_txt.Clear();
            new_office_latitude_txt.Clear();
            new_office_longitude_txt.Clear();
        }
        private Office OfficeFromText(bool hasId)
        {

            string name = new_office_name_txt.Text;
            string address = new_office_address_txt.Text;
            string zip = new_office_zip_txt.Text;
            string city = new_office_city_txt.Text;
            string phone = new_office_phone_txt.Text;
            string fax = new_office_fax_txt.Text;
            string email = new_office_email_txt.Text;
            string latitude = new_office_latitude_txt.Text;
            string longitude = new_office_longitude_txt.Text;
            if (hasId)
            {
                return new Office(office.Id, name, address, zip, city, phone, fax, email, latitude, longitude);
            }
            else
                return new Office(name, address, zip, city, phone, fax, email, latitude, longitude);
        }
        private void OfficeToText(Office office)
        {
            new_office_name_txt.Text = office.Name;
            new_office_address_txt.Text = office.Address;
            new_office_zip_txt.Text = office.ZipCode;
            new_office_city_txt.Text = office.City;
            new_office_phone_txt.Text = office.PhoneNr;
            new_office_fax_txt.Text = office.FaxNr;
            new_office_email_txt.Text = office.Email;
            new_office_latitude_txt.Text = office.Latitude;
            new_office_longitude_txt.Text = office.Longitude;
        }
    }
}
