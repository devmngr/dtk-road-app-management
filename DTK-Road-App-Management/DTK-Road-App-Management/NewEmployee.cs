﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace View
{
    public partial class NewEmployee : Form
    {
        private readonly HomeForm homeForm;
        private Employee employee;
        private IEmployeeController controller;
        private DataTable offices;

        public NewEmployee(HomeForm homeForm, Employee employee)
        {
            InitializeComponent();
            controller = new EmployeeControllerImpl();
            this.homeForm = homeForm;
            this.employee = employee;
            offices = controller.AllOffice();
            FillComboBoxWithOfficeFromTable(offices);
            if (employee != null)
                EmployeeToText(employee);
            enableAdmin();
            DisplayPicture();
        }

        private void employee_admin_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            enableAdmin();
        }
        private void enableAdmin()
        {
            if (!employee_admin_checkBox.Checked)
            {
                employee_username_txt.Enabled = false;
                employee_password_txt.Enabled = false;
            }
            else
            {
                employee_username_txt.Enabled = true;
                employee_password_txt.Enabled = true;
            }
        }

        private void employee_save_btn_Click(object sender, EventArgs e)
        {
            int error = 0;
            Employee temp = null;
            if (employee == null)
            {
                temp = EmployeeFromText(false);
                error = controller.AddEmployee(temp);
            }
            else
            {
                temp = EmployeeFromText(true);
                error = controller.UpdateEmployee(temp);
            }
            ErrorHandler(error);
        }

        private void employee_back_btn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private Employee EmployeeFromText(bool hasId)
        {
            byte[] image = ImageFromPictureBox(employee_picturebox);
            string name = employee_name_txt.Text;
            string position = employee_position_txt.Text;
            Office office = OfficeFromComboBox();
            string email = employee_email_txt.Text;
            string mobile = employee_mobile_txt.Text;
            string direkte = employee_direkte_txt.Text;

            if (employee_admin_checkBox.Checked)
            {
                string username = employee_username_txt.Text;
                string password = employee_password_txt.Text;
                LoginUser usr = new LoginUser(username, password, Rights.ADMIN);
                if (hasId)
                {
                    return new Employee(employee.Id, image, name, position, email, mobile, direkte, office, usr);
                }
                else
                    return new Employee(image, name, position, email, mobile, direkte, office, usr);
            }
            else
            {
                if (hasId)
                {
                    return new Employee(employee.Id, image, name, position, email, mobile, direkte, office);
                }
                else
                    return new Employee(image, name, position, email, mobile, direkte, office);
            }
        }

        private void EmployeeToText(Employee employee)
        {
            employee_name_txt.Text = employee.Name;
            employee_position_txt.Text = employee.Position;
            employee_email_txt.Text = employee.Email;
            employee_mobile_txt.Text = employee.MobileNr;
            employee_direkte_txt.Text = employee.DirekteNr;

            if (employee.HasLoginUser())
            {
                employee_admin_checkBox.Checked = true;
                employee_username_txt.Text = employee.LoginUser.Username;
                employee_password_txt.Text = employee.LoginUser.Password;
            }


            employee_office_combobox.SelectedValue = employee.Office.Name;
        }

        private void ClearTextBoxes()
        {
            employee_name_txt.Clear();
            employee_position_txt.Clear();
            employee_email_txt.Clear();
            employee_mobile_txt.Clear();
            employee_direkte_txt.Clear();
            employee_username_txt.Clear();
            employee_password_txt.Clear();

            employee_admin_checkBox.Checked = false;
        }

        private void ErrorHandler(int error)
        {
            string message = "";

            if (error == 0)
            {
                message = "There was an error adding a new Employee !";
                MessageBox.Show(message, "Employee", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (error == -1)
            {
                message = "Employee already exists!";
                MessageBox.Show(message, "Employee", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (error == -2)
            {
                message = "Username already exists!";
                MessageBox.Show(message, "Employee", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                message = "Employee Saved Successfully !";
                MessageBox.Show(message, "Employee", MessageBoxButtons.OK, MessageBoxIcon.Information);
                homeForm.UpdateEmployeeDataGridView();
                ClearTextBoxes();
                Close();
            }

        }

        private void FillComboBoxWithOfficeFromTable(DataTable table)
        {
            employee_office_combobox.DataSource = offices;
            employee_office_combobox.DisplayMember = ViewColumnNames.name_str;
            employee_office_combobox.ValueMember = ViewColumnNames.name_str;

        }
        private Office OfficeFromComboBox()
        {
            object temp = employee_office_combobox.SelectedValue;
            string officeName = temp.ToString();
            for (int i = 0; i < offices.Rows.Count; i++)
            {
                string str = offices.Rows[i][ViewColumnNames.name_str].ToString();
                if (str.Equals(officeName))
                {
                    int id = Int32.Parse(offices.Rows[i][ViewColumnNames.officeId_str].ToString());
                    string name = offices.Rows[i][ViewColumnNames.name_str].ToString();
                    string address = offices.Rows[i][ViewColumnNames.address_str].ToString();
                    string zipCode = offices.Rows[i][ViewColumnNames.zip_str].ToString();
                    string city = offices.Rows[i][ViewColumnNames.city_str].ToString();
                    string phoneNr = offices.Rows[i][ViewColumnNames.phoneNr_str].ToString();
                    string faxNr = offices.Rows[i][ViewColumnNames.faxNr_str].ToString();
                    string email = offices.Rows[i][ViewColumnNames.email_str].ToString();
                    string latitude = offices.Rows[i][ViewColumnNames.latitude_str].ToString();
                    string longitude = offices.Rows[i][ViewColumnNames.longitude_str].ToString();

                    return new Office(id, name, address, zipCode, city, phoneNr, faxNr, email, latitude, longitude);
                }
            }
            return null;
        }

        private void employee_picturebox_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.jpg, *.jpeg,*.png) | *.jpg; *.jpeg; *.png";
            openFileDialog.FilterIndex = 2;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;
                employee_picturebox.Image = Image.FromFile(filePath);
                employee_picturebox.SizeMode = PictureBoxSizeMode.Zoom;
            }

        }

        private void DisplayPicture()
        {
            if (employee != null)
                employee_picturebox.Image = ByteArrayToImage(employee.Picture);
            else
                employee_picturebox.Image = Properties.Resources.ic_account_circle_black_24dp;

        }

        private byte[] ImageFromPictureBox(PictureBox pictureBox)
        {
            Size size = new Size(96, 96);
            Image temp = ResizeImage(pictureBox.Image, size, true);
            return ImageToByteArray(temp);
        }
        private Image ByteArrayToImage(byte[] bytes)
        {
            if (bytes == null)
                bytes = ImageToByteArray(Properties.Resources.ic_account_circle_black_24dp);

            Image temp;
            using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
            {
                ms.Write(bytes, 0, bytes.Length);
                temp = Image.FromStream(ms, true);
            }
            return temp;
        }

        private byte[] ImageToByteArray(Image image)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                stream.Close();
                byteArray = stream.ToArray();
            }
            return byteArray;
        }
        private Image ResizeImage(Image image, Size size,
            bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }
            Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }
    }
}
