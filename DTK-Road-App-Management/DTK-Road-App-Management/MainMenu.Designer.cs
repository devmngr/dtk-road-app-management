﻿namespace View
{
    partial class HomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.home_exit_btn = new System.Windows.Forms.Button();
            this.home_office_tab = new System.Windows.Forms.TabPage();
            this.office_dataGridView = new System.Windows.Forms.DataGridView();
            this.home_employee_tab = new System.Windows.Forms.TabPage();
            this.employee_dataGridView = new System.Windows.Forms.DataGridView();
            this.home_truck_wash_tab = new System.Windows.Forms.TabPage();
            this.truck_wash_dataGridView = new System.Windows.Forms.DataGridView();
            this.home_gas_stations_tab = new System.Windows.Forms.TabPage();
            this.gas_station_dataGridView = new System.Windows.Forms.DataGridView();
            this.home_save_btn = new System.Windows.Forms.Button();
            this.home_delete_btn = new System.Windows.Forms.Button();
            this.home_csv_upload_btn = new System.Windows.Forms.Button();
            this.home_logout_btn = new System.Windows.Forms.Button();
            this.home_tab_control = new System.Windows.Forms.TabControl();
            this.home_broker_tab = new System.Windows.Forms.TabPage();
            this.broker_dataGridView = new System.Windows.Forms.DataGridView();
            this.home_haulier_tab = new System.Windows.Forms.TabPage();
            this.haulier_dataGridView = new System.Windows.Forms.DataGridView();
            this.home_login_info_tab = new System.Windows.Forms.TabPage();
            this.login_user_dataGridView = new System.Windows.Forms.DataGridView();
            this.home_new_btn = new System.Windows.Forms.Button();
            this.home_edit_btn = new System.Windows.Forms.Button();
            this.home_office_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.office_dataGridView)).BeginInit();
            this.home_employee_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employee_dataGridView)).BeginInit();
            this.home_truck_wash_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.truck_wash_dataGridView)).BeginInit();
            this.home_gas_stations_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gas_station_dataGridView)).BeginInit();
            this.home_tab_control.SuspendLayout();
            this.home_broker_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.broker_dataGridView)).BeginInit();
            this.home_haulier_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.haulier_dataGridView)).BeginInit();
            this.home_login_info_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.login_user_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // home_exit_btn
            // 
            this.home_exit_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.home_exit_btn.Location = new System.Drawing.Point(2019, 1360);
            this.home_exit_btn.Margin = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.home_exit_btn.Name = "home_exit_btn";
            this.home_exit_btn.Size = new System.Drawing.Size(200, 60);
            this.home_exit_btn.TabIndex = 5;
            this.home_exit_btn.Text = "Exit";
            this.home_exit_btn.UseVisualStyleBackColor = true;
            this.home_exit_btn.Click += new System.EventHandler(this.home_exit_btn_Click);
            // 
            // home_office_tab
            // 
            this.home_office_tab.Controls.Add(this.office_dataGridView);
            this.home_office_tab.Location = new System.Drawing.Point(10, 55);
            this.home_office_tab.Name = "home_office_tab";
            this.home_office_tab.Padding = new System.Windows.Forms.Padding(3);
            this.home_office_tab.Size = new System.Drawing.Size(2288, 1225);
            this.home_office_tab.TabIndex = 3;
            this.home_office_tab.Text = "Office";
            this.home_office_tab.UseVisualStyleBackColor = true;
            // 
            // office_dataGridView
            // 
            this.office_dataGridView.AllowUserToAddRows = false;
            this.office_dataGridView.AllowUserToDeleteRows = false;
            this.office_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.office_dataGridView.Location = new System.Drawing.Point(6, 6);
            this.office_dataGridView.MultiSelect = false;
            this.office_dataGridView.Name = "office_dataGridView";
            this.office_dataGridView.ReadOnly = true;
            this.office_dataGridView.RowTemplate.Height = 40;
            this.office_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.office_dataGridView.Size = new System.Drawing.Size(2276, 1213);
            this.office_dataGridView.TabIndex = 0;
            // 
            // home_employee_tab
            // 
            this.home_employee_tab.Controls.Add(this.employee_dataGridView);
            this.home_employee_tab.Location = new System.Drawing.Point(10, 55);
            this.home_employee_tab.Name = "home_employee_tab";
            this.home_employee_tab.Padding = new System.Windows.Forms.Padding(3);
            this.home_employee_tab.Size = new System.Drawing.Size(2288, 1225);
            this.home_employee_tab.TabIndex = 2;
            this.home_employee_tab.Text = "Employee";
            this.home_employee_tab.UseVisualStyleBackColor = true;
            // 
            // employee_dataGridView
            // 
            this.employee_dataGridView.AllowUserToAddRows = false;
            this.employee_dataGridView.AllowUserToDeleteRows = false;
            this.employee_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.employee_dataGridView.Location = new System.Drawing.Point(6, 6);
            this.employee_dataGridView.MultiSelect = false;
            this.employee_dataGridView.Name = "employee_dataGridView";
            this.employee_dataGridView.ReadOnly = true;
            this.employee_dataGridView.RowTemplate.Height = 40;
            this.employee_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.employee_dataGridView.Size = new System.Drawing.Size(2276, 1213);
            this.employee_dataGridView.TabIndex = 0;
            // 
            // home_truck_wash_tab
            // 
            this.home_truck_wash_tab.Controls.Add(this.truck_wash_dataGridView);
            this.home_truck_wash_tab.Location = new System.Drawing.Point(10, 55);
            this.home_truck_wash_tab.Name = "home_truck_wash_tab";
            this.home_truck_wash_tab.Padding = new System.Windows.Forms.Padding(3);
            this.home_truck_wash_tab.Size = new System.Drawing.Size(2288, 1225);
            this.home_truck_wash_tab.TabIndex = 1;
            this.home_truck_wash_tab.Text = "Truck Wash";
            this.home_truck_wash_tab.UseVisualStyleBackColor = true;
            // 
            // truck_wash_dataGridView
            // 
            this.truck_wash_dataGridView.AllowUserToAddRows = false;
            this.truck_wash_dataGridView.AllowUserToDeleteRows = false;
            this.truck_wash_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.truck_wash_dataGridView.Location = new System.Drawing.Point(6, 6);
            this.truck_wash_dataGridView.MultiSelect = false;
            this.truck_wash_dataGridView.Name = "truck_wash_dataGridView";
            this.truck_wash_dataGridView.ReadOnly = true;
            this.truck_wash_dataGridView.RowTemplate.Height = 40;
            this.truck_wash_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.truck_wash_dataGridView.Size = new System.Drawing.Size(2276, 1213);
            this.truck_wash_dataGridView.TabIndex = 0;
            // 
            // home_gas_stations_tab
            // 
            this.home_gas_stations_tab.Controls.Add(this.gas_station_dataGridView);
            this.home_gas_stations_tab.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.home_gas_stations_tab.Location = new System.Drawing.Point(10, 55);
            this.home_gas_stations_tab.Name = "home_gas_stations_tab";
            this.home_gas_stations_tab.Padding = new System.Windows.Forms.Padding(3);
            this.home_gas_stations_tab.Size = new System.Drawing.Size(2288, 1225);
            this.home_gas_stations_tab.TabIndex = 0;
            this.home_gas_stations_tab.Text = "Gas Stations";
            this.home_gas_stations_tab.UseVisualStyleBackColor = true;
            // 
            // gas_station_dataGridView
            // 
            this.gas_station_dataGridView.AllowUserToAddRows = false;
            this.gas_station_dataGridView.AllowUserToDeleteRows = false;
            this.gas_station_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gas_station_dataGridView.Location = new System.Drawing.Point(6, 6);
            this.gas_station_dataGridView.MultiSelect = false;
            this.gas_station_dataGridView.Name = "gas_station_dataGridView";
            this.gas_station_dataGridView.ReadOnly = true;
            this.gas_station_dataGridView.RowTemplate.Height = 40;
            this.gas_station_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gas_station_dataGridView.Size = new System.Drawing.Size(2276, 1213);
            this.gas_station_dataGridView.TabIndex = 0;
            // 
            // home_save_btn
            // 
            this.home_save_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.home_save_btn.Location = new System.Drawing.Point(1539, 1360);
            this.home_save_btn.Margin = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.home_save_btn.Name = "home_save_btn";
            this.home_save_btn.Size = new System.Drawing.Size(200, 60);
            this.home_save_btn.TabIndex = 5;
            this.home_save_btn.Text = "Save";
            this.home_save_btn.UseVisualStyleBackColor = true;
            this.home_save_btn.Click += new System.EventHandler(this.home_save_btn_Click);
            // 
            // home_delete_btn
            // 
            this.home_delete_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.home_delete_btn.Location = new System.Drawing.Point(520, 1360);
            this.home_delete_btn.Margin = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.home_delete_btn.Name = "home_delete_btn";
            this.home_delete_btn.Size = new System.Drawing.Size(200, 60);
            this.home_delete_btn.TabIndex = 6;
            this.home_delete_btn.Text = "Delete";
            this.home_delete_btn.UseVisualStyleBackColor = true;
            this.home_delete_btn.Click += new System.EventHandler(this.home_delete_btn_Click);
            // 
            // home_csv_upload_btn
            // 
            this.home_csv_upload_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.home_csv_upload_btn.Location = new System.Drawing.Point(760, 1360);
            this.home_csv_upload_btn.Margin = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.home_csv_upload_btn.Name = "home_csv_upload_btn";
            this.home_csv_upload_btn.Size = new System.Drawing.Size(246, 60);
            this.home_csv_upload_btn.TabIndex = 2;
            this.home_csv_upload_btn.Text = "CSV Upload";
            this.home_csv_upload_btn.UseVisualStyleBackColor = true;
            this.home_csv_upload_btn.Click += new System.EventHandler(this.home_csv_upload_btn_Click);
            // 
            // home_logout_btn
            // 
            this.home_logout_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.home_logout_btn.Location = new System.Drawing.Point(1779, 1360);
            this.home_logout_btn.Margin = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.home_logout_btn.Name = "home_logout_btn";
            this.home_logout_btn.Size = new System.Drawing.Size(200, 60);
            this.home_logout_btn.TabIndex = 7;
            this.home_logout_btn.Text = "Logout";
            this.home_logout_btn.UseVisualStyleBackColor = true;
            // 
            // home_tab_control
            // 
            this.home_tab_control.Controls.Add(this.home_gas_stations_tab);
            this.home_tab_control.Controls.Add(this.home_truck_wash_tab);
            this.home_tab_control.Controls.Add(this.home_employee_tab);
            this.home_tab_control.Controls.Add(this.home_office_tab);
            this.home_tab_control.Controls.Add(this.home_broker_tab);
            this.home_tab_control.Controls.Add(this.home_haulier_tab);
            this.home_tab_control.Controls.Add(this.home_login_info_tab);
            this.home_tab_control.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.home_tab_control.Location = new System.Drawing.Point(30, 30);
            this.home_tab_control.Margin = new System.Windows.Forms.Padding(10);
            this.home_tab_control.Name = "home_tab_control";
            this.home_tab_control.SelectedIndex = 0;
            this.home_tab_control.Size = new System.Drawing.Size(2308, 1290);
            this.home_tab_control.TabIndex = 6;
            // 
            // home_broker_tab
            // 
            this.home_broker_tab.Controls.Add(this.broker_dataGridView);
            this.home_broker_tab.Location = new System.Drawing.Point(10, 55);
            this.home_broker_tab.Name = "home_broker_tab";
            this.home_broker_tab.Padding = new System.Windows.Forms.Padding(3);
            this.home_broker_tab.Size = new System.Drawing.Size(2288, 1225);
            this.home_broker_tab.TabIndex = 4;
            this.home_broker_tab.Text = "Broker";
            this.home_broker_tab.UseVisualStyleBackColor = true;
            // 
            // broker_dataGridView
            // 
            this.broker_dataGridView.AllowUserToAddRows = false;
            this.broker_dataGridView.AllowUserToDeleteRows = false;
            this.broker_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.broker_dataGridView.Location = new System.Drawing.Point(6, 6);
            this.broker_dataGridView.MultiSelect = false;
            this.broker_dataGridView.Name = "broker_dataGridView";
            this.broker_dataGridView.ReadOnly = true;
            this.broker_dataGridView.RowTemplate.Height = 40;
            this.broker_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.broker_dataGridView.Size = new System.Drawing.Size(2276, 1213);
            this.broker_dataGridView.TabIndex = 0;
            // 
            // home_haulier_tab
            // 
            this.home_haulier_tab.Controls.Add(this.haulier_dataGridView);
            this.home_haulier_tab.Location = new System.Drawing.Point(10, 55);
            this.home_haulier_tab.Name = "home_haulier_tab";
            this.home_haulier_tab.Padding = new System.Windows.Forms.Padding(3);
            this.home_haulier_tab.Size = new System.Drawing.Size(2288, 1225);
            this.home_haulier_tab.TabIndex = 5;
            this.home_haulier_tab.Text = "Haulier";
            this.home_haulier_tab.UseVisualStyleBackColor = true;
            // 
            // haulier_dataGridView
            // 
            this.haulier_dataGridView.AllowUserToAddRows = false;
            this.haulier_dataGridView.AllowUserToDeleteRows = false;
            this.haulier_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.haulier_dataGridView.Location = new System.Drawing.Point(6, 6);
            this.haulier_dataGridView.MultiSelect = false;
            this.haulier_dataGridView.Name = "haulier_dataGridView";
            this.haulier_dataGridView.ReadOnly = true;
            this.haulier_dataGridView.RowTemplate.Height = 40;
            this.haulier_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.haulier_dataGridView.Size = new System.Drawing.Size(2276, 1213);
            this.haulier_dataGridView.TabIndex = 0;
            // 
            // home_login_info_tab
            // 
            this.home_login_info_tab.Controls.Add(this.login_user_dataGridView);
            this.home_login_info_tab.Location = new System.Drawing.Point(10, 55);
            this.home_login_info_tab.Name = "home_login_info_tab";
            this.home_login_info_tab.Padding = new System.Windows.Forms.Padding(3);
            this.home_login_info_tab.Size = new System.Drawing.Size(2288, 1225);
            this.home_login_info_tab.TabIndex = 6;
            this.home_login_info_tab.Text = "User";
            this.home_login_info_tab.UseVisualStyleBackColor = true;
            // 
            // login_user_dataGridView
            // 
            this.login_user_dataGridView.AllowUserToAddRows = false;
            this.login_user_dataGridView.AllowUserToDeleteRows = false;
            this.login_user_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.login_user_dataGridView.Location = new System.Drawing.Point(6, 6);
            this.login_user_dataGridView.MultiSelect = false;
            this.login_user_dataGridView.Name = "login_user_dataGridView";
            this.login_user_dataGridView.ReadOnly = true;
            this.login_user_dataGridView.RowTemplate.Height = 40;
            this.login_user_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.login_user_dataGridView.Size = new System.Drawing.Size(2276, 1213);
            this.login_user_dataGridView.TabIndex = 0;
            // 
            // home_new_btn
            // 
            this.home_new_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.home_new_btn.Location = new System.Drawing.Point(40, 1360);
            this.home_new_btn.Margin = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.home_new_btn.Name = "home_new_btn";
            this.home_new_btn.Size = new System.Drawing.Size(200, 60);
            this.home_new_btn.TabIndex = 8;
            this.home_new_btn.Text = "New";
            this.home_new_btn.UseVisualStyleBackColor = true;
            this.home_new_btn.Click += new System.EventHandler(this.home_new_btn_Click);
            // 
            // home_edit_btn
            // 
            this.home_edit_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.home_edit_btn.Location = new System.Drawing.Point(280, 1360);
            this.home_edit_btn.Margin = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.home_edit_btn.Name = "home_edit_btn";
            this.home_edit_btn.Size = new System.Drawing.Size(200, 60);
            this.home_edit_btn.TabIndex = 9;
            this.home_edit_btn.Text = "Edit";
            this.home_edit_btn.UseVisualStyleBackColor = true;
            this.home_edit_btn.Click += new System.EventHandler(this.home_edit_btn_Click);
            // 
            // HomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(240F, 240F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(2368, 1512);
            this.Controls.Add(this.home_edit_btn);
            this.Controls.Add(this.home_new_btn);
            this.Controls.Add(this.home_save_btn);
            this.Controls.Add(this.home_delete_btn);
            this.Controls.Add(this.home_logout_btn);
            this.Controls.Add(this.home_csv_upload_btn);
            this.Controls.Add(this.home_exit_btn);
            this.Controls.Add(this.home_tab_control);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "HomeForm";
            this.Padding = new System.Windows.Forms.Padding(20);
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "DTK Road - Home";
            this.home_office_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.office_dataGridView)).EndInit();
            this.home_employee_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.employee_dataGridView)).EndInit();
            this.home_truck_wash_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.truck_wash_dataGridView)).EndInit();
            this.home_gas_stations_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gas_station_dataGridView)).EndInit();
            this.home_tab_control.ResumeLayout(false);
            this.home_broker_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.broker_dataGridView)).EndInit();
            this.home_haulier_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.haulier_dataGridView)).EndInit();
            this.home_login_info_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.login_user_dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button home_exit_btn;
        private System.Windows.Forms.TabPage home_office_tab;
        private System.Windows.Forms.TabPage home_employee_tab;
        private System.Windows.Forms.TabPage home_truck_wash_tab;
        private System.Windows.Forms.TabPage home_gas_stations_tab;
        private System.Windows.Forms.Button home_logout_btn;
        private System.Windows.Forms.Button home_delete_btn;
        private System.Windows.Forms.Button home_save_btn;
        private System.Windows.Forms.Button home_csv_upload_btn;
        private System.Windows.Forms.TabControl home_tab_control;
        private System.Windows.Forms.Button home_new_btn;
        private System.Windows.Forms.Button home_edit_btn;
        private System.Windows.Forms.DataGridView gas_station_dataGridView;
        private System.Windows.Forms.DataGridView truck_wash_dataGridView;
        private System.Windows.Forms.DataGridView employee_dataGridView;
        private System.Windows.Forms.DataGridView office_dataGridView;
        private System.Windows.Forms.TabPage home_broker_tab;
        private System.Windows.Forms.DataGridView broker_dataGridView;
        private System.Windows.Forms.TabPage home_haulier_tab;
        private System.Windows.Forms.TabPage home_login_info_tab;
        private System.Windows.Forms.DataGridView haulier_dataGridView;
        private System.Windows.Forms.DataGridView login_user_dataGridView;
    }
}

