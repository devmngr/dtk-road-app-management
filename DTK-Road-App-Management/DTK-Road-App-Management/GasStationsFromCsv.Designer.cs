﻿namespace View
{
    partial class GasStationsFromCsv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gsCSV_dataGridView = new System.Windows.Forms.DataGridView();
            this.gsCSV_back_btn = new System.Windows.Forms.Button();
            this.gsCSV_save_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gsCSV_dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // gsCSV_dataGridView
            // 
            this.gsCSV_dataGridView.AllowUserToAddRows = false;
            this.gsCSV_dataGridView.AllowUserToDeleteRows = false;
            this.gsCSV_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gsCSV_dataGridView.Location = new System.Drawing.Point(23, 23);
            this.gsCSV_dataGridView.Name = "gsCSV_dataGridView";
            this.gsCSV_dataGridView.ReadOnly = true;
            this.gsCSV_dataGridView.RowTemplate.Height = 40;
            this.gsCSV_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gsCSV_dataGridView.Size = new System.Drawing.Size(2173, 1199);
            this.gsCSV_dataGridView.TabIndex = 0;
            // 
            // gsCSV_back_btn
            // 
            this.gsCSV_back_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gsCSV_back_btn.Location = new System.Drawing.Point(70, 1325);
            this.gsCSV_back_btn.Margin = new System.Windows.Forms.Padding(50, 100, 0, 20);
            this.gsCSV_back_btn.Name = "gsCSV_back_btn";
            this.gsCSV_back_btn.Size = new System.Drawing.Size(200, 60);
            this.gsCSV_back_btn.TabIndex = 1;
            this.gsCSV_back_btn.Text = "Back";
            this.gsCSV_back_btn.UseVisualStyleBackColor = true;
            this.gsCSV_back_btn.Click += new System.EventHandler(this.gsCSV_back_btn_Click);
            // 
            // gsCSV_save_btn
            // 
            this.gsCSV_save_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gsCSV_save_btn.Location = new System.Drawing.Point(2023, 1325);
            this.gsCSV_save_btn.Margin = new System.Windows.Forms.Padding(0, 100, 50, 20);
            this.gsCSV_save_btn.Name = "gsCSV_save_btn";
            this.gsCSV_save_btn.Size = new System.Drawing.Size(200, 60);
            this.gsCSV_save_btn.TabIndex = 2;
            this.gsCSV_save_btn.Text = "Save";
            this.gsCSV_save_btn.UseVisualStyleBackColor = true;
            this.gsCSV_save_btn.Click += new System.EventHandler(this.gsCSV_save_btn_Click);
            // 
            // GasStationsFromCsv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(240F, 240F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(2219, 1425);
            this.Controls.Add(this.gsCSV_save_btn);
            this.Controls.Add(this.gsCSV_back_btn);
            this.Controls.Add(this.gsCSV_dataGridView);
            this.MaximizeBox = false;
            this.Name = "GasStationsFromCsv";
            this.Padding = new System.Windows.Forms.Padding(20);
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "GasStationsFromCsv";
            ((System.ComponentModel.ISupportInitialize)(this.gsCSV_dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gsCSV_dataGridView;
        private System.Windows.Forms.Button gsCSV_back_btn;
        private System.Windows.Forms.Button gsCSV_save_btn;
    }
}