﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace View
{
    public partial class NewHaulier : Form
    {
        private readonly HomeForm homeForm;
        private Haulier haulier;
        private IHaulierController controller;
        private DataTable brokers;

        public NewHaulier(HomeForm homeForm, Haulier haulier)
        {
            InitializeComponent();
            controller = new HaulierControllerImpl();
            this.homeForm = homeForm;
            this.haulier = haulier;
            brokers = controller.AllBroker();
            FillComboBoxWithBrokerFromTable(brokers);

            if (haulier != null)
                HaulierToText(haulier);
        }

        private void new_haulier_save_btn_Click(object sender, EventArgs e)
        {
            int error = 0;
            Haulier temp = null;
            if (haulier == null)
            {
                temp = HaulierFromText(false);
                error = controller.AddHaulier(temp);
            }
            else
            {
                temp = HaulierFromText(true);
                error = controller.UpdateHaulier(temp);
            }
            ErrorHandler(error);
        }
        private void new_haulier_back_btn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private Haulier HaulierFromText(bool hasId)
        {

            string name = new_haulier_name_txt.Text;
            string email = new_haulier_email_txt.Text;
            string truckNr = new_haulier_truck_nr_txt.Text;

            Broker broker = BrokerFromComboBox();
            string username = new_haulier_username_txt.Text;
            string password = new_haulier_password_txt.Text;
            LoginUser usr = new LoginUser(username, password, Rights.USER);
            if (hasId)
            {

                return new Haulier(haulier.Id, name, email, truckNr, usr, broker);
            }


            return new Haulier(name, email, truckNr, usr, broker);
        }

        private void HaulierToText(Haulier haulier)
        {
            new_haulier_name_txt.Text = haulier.Name;
            new_haulier_email_txt.Text = haulier.Email;
            new_haulier_truck_nr_txt.Text = haulier.TruckNr;
            new_haulier_username_txt.Text = haulier.LoginUser.Username;
            new_haulier_password_txt.Text = haulier.LoginUser.Password;
            new_haulier_broker_comboBox.SelectedValue = haulier.Broker.Name;

        }
        private void ClearTextBoxes()
        {
            new_haulier_name_txt.Clear();
            new_haulier_email_txt.Clear();
            new_haulier_truck_nr_txt.Clear();

            new_haulier_username_txt.Clear();
            new_haulier_password_txt.Clear();
            new_haulier_other_txt.Clear();
        }
        private void ResetRadioButtons()
        {
            new_haulier_dtk_road_radioBtn.Checked = false;
            new_haulier_broker_radioBtn.Checked = false;
            new_haulier_other_radioBtn.Checked = false;
        }
        private void ErrorHandler(int error)
        {
            string message = "";

            if (error == 0)
            {
                message = "There was an error adding a new Haulier !";
                MessageBox.Show(message, "Haulier", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (error == -1)
            {
                message = "Haulier already exists!";
                MessageBox.Show(message, "Haulier", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (error == -2)
            {
                message = "Username already exists!";
                MessageBox.Show(message, "Haulier", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                message = "Haulier Saved Successfully !";
                MessageBox.Show(message, "Haulier", MessageBoxButtons.OK, MessageBoxIcon.Information);
                homeForm.UpdateHaulierDataGridView();
                ClearTextBoxes();
                ResetRadioButtons();
                Close();
            }

            /*
             string message = "";

            if (error == 0)
            {
                message = "There was an error adding a new Broker !";
                MessageBox.Show(message, "Broker", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (error == -1)
            {
                message = "Broker already exists!";
                MessageBox.Show(message, "Broker", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (error == -2)
            {
                message = "Username already exists!";
                MessageBox.Show(message, "Broker", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                message = "Broker Saved Successfully !";
                MessageBox.Show(message, "Broker", MessageBoxButtons.OK, MessageBoxIcon.Information);
                homeForm.UpdateBrokerDataGridView();
                ClearTextBoxes();
                Close();
            }
             
             */
        }

        private void FillComboBoxWithBrokerFromTable(DataTable table)
        {
            new_haulier_broker_comboBox.DataSource = brokers;
            new_haulier_broker_comboBox.DisplayMember = ViewColumnNames.name_str;
            new_haulier_broker_comboBox.ValueMember = ViewColumnNames.name_str;


            /*
            new_haulier_broker_comboBox.Items.Add("");
            for (int i = 0; i < table.Rows.Count; i++)
            {
                new_haulier_broker_comboBox.Items.Add(table.Rows[i][ViewColumnNames.name_str]);
            }
            */
        }
        private Broker BrokerFromComboBox()
        {
            object temp = new_haulier_broker_comboBox.SelectedValue;
            string brokerName = temp.ToString();
            for (int i = 0; i < brokers.Rows.Count; i++)
            {
                string str = brokers.Rows[i][ViewColumnNames.name_str].ToString();
                if (str.Equals(brokerName))
                {
                    int id = Int32.Parse(brokers.Rows[i][ViewColumnNames.brokerId_str].ToString());
                    string email = brokers.Rows[i][ViewColumnNames.email_str].ToString();
                    return new Broker(id, brokerName, email);
                }
            }
            return null;
        }
    }
}
