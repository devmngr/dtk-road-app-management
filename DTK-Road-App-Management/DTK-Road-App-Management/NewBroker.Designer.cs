﻿namespace View
{
    partial class NewBroker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.new_broker_name_txt = new System.Windows.Forms.TextBox();
            this.new_broker_email_txt = new System.Windows.Forms.TextBox();
            this.new_broker_username_txt = new System.Windows.Forms.TextBox();
            this.new_broker_password_txt = new System.Windows.Forms.TextBox();
            this.new_broker_email_to_txt = new System.Windows.Forms.TextBox();
            this.new_broker_back_btn = new System.Windows.Forms.Button();
            this.new_broker_save_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(40, 116);
            this.label2.Margin = new System.Windows.Forms.Padding(20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 39);
            this.label2.TabIndex = 1;
            this.label2.Text = "Email";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(40, 194);
            this.label3.Margin = new System.Windows.Forms.Padding(20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 39);
            this.label3.TabIndex = 2;
            this.label3.Text = "Username";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(40, 272);
            this.label4.Margin = new System.Windows.Forms.Padding(20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 39);
            this.label4.TabIndex = 3;
            this.label4.Text = "Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(40, 352);
            this.label5.Margin = new System.Windows.Forms.Padding(20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(415, 39);
            this.label5.TabIndex = 4;
            this.label5.Text = "Send Documents to email:";
            // 
            // new_broker_name_txt
            // 
            this.new_broker_name_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_broker_name_txt.Location = new System.Drawing.Point(254, 38);
            this.new_broker_name_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_broker_name_txt.Name = "new_broker_name_txt";
            this.new_broker_name_txt.Size = new System.Drawing.Size(500, 41);
            this.new_broker_name_txt.TabIndex = 5;
            // 
            // new_broker_email_txt
            // 
            this.new_broker_email_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_broker_email_txt.Location = new System.Drawing.Point(254, 118);
            this.new_broker_email_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_broker_email_txt.Name = "new_broker_email_txt";
            this.new_broker_email_txt.Size = new System.Drawing.Size(500, 41);
            this.new_broker_email_txt.TabIndex = 6;
            // 
            // new_broker_username_txt
            // 
            this.new_broker_username_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_broker_username_txt.Location = new System.Drawing.Point(254, 196);
            this.new_broker_username_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_broker_username_txt.Name = "new_broker_username_txt";
            this.new_broker_username_txt.Size = new System.Drawing.Size(500, 41);
            this.new_broker_username_txt.TabIndex = 7;
            // 
            // new_broker_password_txt
            // 
            this.new_broker_password_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_broker_password_txt.Location = new System.Drawing.Point(254, 274);
            this.new_broker_password_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_broker_password_txt.Name = "new_broker_password_txt";
            this.new_broker_password_txt.Size = new System.Drawing.Size(500, 41);
            this.new_broker_password_txt.TabIndex = 8;
            // 
            // new_broker_email_to_txt
            // 
            this.new_broker_email_to_txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_broker_email_to_txt.Location = new System.Drawing.Point(254, 431);
            this.new_broker_email_to_txt.Margin = new System.Windows.Forms.Padding(20);
            this.new_broker_email_to_txt.Name = "new_broker_email_to_txt";
            this.new_broker_email_to_txt.Size = new System.Drawing.Size(500, 41);
            this.new_broker_email_to_txt.TabIndex = 9;
            // 
            // new_broker_back_btn
            // 
            this.new_broker_back_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_broker_back_btn.Location = new System.Drawing.Point(70, 589);
            this.new_broker_back_btn.Margin = new System.Windows.Forms.Padding(50, 100, 0, 20);
            this.new_broker_back_btn.Name = "new_broker_back_btn";
            this.new_broker_back_btn.Size = new System.Drawing.Size(200, 60);
            this.new_broker_back_btn.TabIndex = 10;
            this.new_broker_back_btn.Text = "Back";
            this.new_broker_back_btn.UseVisualStyleBackColor = true;
            this.new_broker_back_btn.Click += new System.EventHandler(this.new_broker_back_btn_Click);
            // 
            // new_broker_save_btn
            // 
            this.new_broker_save_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_broker_save_btn.Location = new System.Drawing.Point(524, 589);
            this.new_broker_save_btn.Margin = new System.Windows.Forms.Padding(0, 100, 50, 20);
            this.new_broker_save_btn.Name = "new_broker_save_btn";
            this.new_broker_save_btn.Size = new System.Drawing.Size(200, 60);
            this.new_broker_save_btn.TabIndex = 11;
            this.new_broker_save_btn.Text = "Save";
            this.new_broker_save_btn.UseVisualStyleBackColor = true;
            this.new_broker_save_btn.Click += new System.EventHandler(this.new_broker_save_btn_Click);
            // 
            // NewBroker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(240F, 240F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(814, 721);
            this.Controls.Add(this.new_broker_save_btn);
            this.Controls.Add(this.new_broker_back_btn);
            this.Controls.Add(this.new_broker_email_to_txt);
            this.Controls.Add(this.new_broker_password_txt);
            this.Controls.Add(this.new_broker_username_txt);
            this.Controls.Add(this.new_broker_email_txt);
            this.Controls.Add(this.new_broker_name_txt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "NewBroker";
            this.Padding = new System.Windows.Forms.Padding(20);
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "NewBroker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox new_broker_name_txt;
        private System.Windows.Forms.TextBox new_broker_email_txt;
        private System.Windows.Forms.TextBox new_broker_username_txt;
        private System.Windows.Forms.TextBox new_broker_password_txt;
        private System.Windows.Forms.TextBox new_broker_email_to_txt;
        private System.Windows.Forms.Button new_broker_back_btn;
        private System.Windows.Forms.Button new_broker_save_btn;
    }
}