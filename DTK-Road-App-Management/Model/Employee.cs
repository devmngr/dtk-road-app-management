﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Model
{
    public class Employee
    {
        private int id;
        private byte[] picture;
        private string name, position, email, mobileNr, direkteNr;
        private Office office;
        private LoginUser loginUser;
        private bool hasId;

        public Employee(byte[] picture, string name, string position, string email, string mobileNr, string direkteNr, Office office)
        {
            this.picture = picture;
            this.name = name;
            this.position = position;
            this.email = email;
            this.mobileNr = mobileNr;
            this.direkteNr = direkteNr;
            this.office = office;
            hasId = false;
            this.loginUser = null;
        }

        public Employee(byte[] picture, string name, string position, string email, string mobileNr, string direkteNr, Office office, LoginUser loginUser)
        {
            this.picture = picture;
            this.name = name;
            this.position = position;
            this.email = email;
            this.mobileNr = mobileNr;
            this.direkteNr = direkteNr;
            this.office = office;
            this.loginUser = loginUser;
            hasId = false;
        }

        public Employee(int id, byte[] picture, string name, string position, string email, string mobileNr, string direkteNr, Office office)
        {
            this.id = id;
            this.picture = picture;
            this.name = name;
            this.position = position;
            this.email = email;
            this.mobileNr = mobileNr;
            this.direkteNr = direkteNr;
            this.office = office;
            hasId = true;
            this.loginUser = null;
        }

        public Employee(int id, byte[] picture, string name, string position, string email, string mobileNr, string direkteNr, Office office, LoginUser loginUser)
        {
            this.id = id;
            this.picture = picture;
            this.name = name;
            this.position = position;
            this.email = email;
            this.mobileNr = mobileNr;
            this.direkteNr = direkteNr;
            this.office = office;
            this.loginUser = loginUser;
            hasId = true;
        }

        public string Name { get => name; set => name = value; }
        public string Position { get => position; set => position = value; }
        public string Email { get => email; set => email = value; }
        public string MobileNr { get => mobileNr; set => mobileNr = value; }
        public string DirekteNr { get => direkteNr; set => direkteNr = value; }
        public Office Office { get => office; set => office = value; }
        public LoginUser LoginUser { get => loginUser; set => loginUser = value; }
        public bool HasId { get => hasId; set => hasId = value; }
        public int Id { get => id; set => id = value; }
        public byte[] Picture { get => picture; set => picture = value; }
        public bool HasLoginUser()
        {
            return loginUser != null;
        }

        public override string ToString()
        {
            return name
                + "\n" + position
                + "\n" + email
                + "\n" + mobileNr
                + "\n" + direkteNr
                + "\n" + office.Name
                + "\n" + loginUser;
        }


    }
}
