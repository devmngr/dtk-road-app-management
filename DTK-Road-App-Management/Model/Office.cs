﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Office
    {
        private int id;
        private string name, address, zipCode, city, phoneNr, faxNr, email, latitude, longitude;
        private bool hasId;
        public Office(int id, string name)
        {
            this.id = id;
            this.name = name;
            this.address = "";
            this.zipCode = "";
            this.city = "";
            this.phoneNr = "";
            this.faxNr = "";
            this.email = "";
            this.latitude = "";
            this.longitude = "";
            hasId = true;
        }
        public Office(string name, string address, string zipCode, string city, string phoneNr, string faxNr, string email, string latitude, string longitude)
        {
            this.name = name;
            this.address = address;
            this.zipCode = zipCode;
            this.city = city;
            this.phoneNr = phoneNr;
            this.faxNr = faxNr;
            this.email = email;
            this.latitude = latitude;
            this.longitude = longitude;
            hasId = false;
        }

        public Office(int id, string name, string address, string zipCode, string city, string phoneNr, string faxNr, string email, string latitude, string longitude)
        {
            this.id = id;
            this.name = name;
            this.address = address;
            this.zipCode = zipCode;
            this.city = city;
            this.phoneNr = phoneNr;
            this.faxNr = faxNr;
            this.email = email;
            this.latitude = latitude;
            this.longitude = longitude;
            hasId = true;
        }

        public string Name { get => name; set => name = value; }
        public string Address { get => address; set => address = value; }
        public string ZipCode { get => zipCode; set => zipCode = value; }
        public string City { get => city; set => city = value; }
        public string PhoneNr { get => phoneNr; set => phoneNr = value; }
        public string FaxNr { get => faxNr; set => faxNr = value; }
        public string Email { get => email; set => email = value; }
        public string Latitude { get => latitude; set => latitude = value; }
        public string Longitude { get => longitude; set => longitude = value; }
        public int Id { get => id; set => id = value; }
        public bool HasId { get => hasId; set => hasId = value; }

        public override string ToString()
        {
            return name
                + "\n" + address
                + "\n" + zipCode
                + "\n" + city
                + "\n" + phoneNr
                + "\n" + faxNr
                + "\n" + email
                + "\n" + latitude
                + "\n" + longitude;
        }
    }
}
