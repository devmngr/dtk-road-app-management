﻿

namespace Model
{
    public class TruckWash
    {
        private string name, address, zip, city, phone, email, openingHours, remarks, website, longitude, latitude;
        private string truckTrailerStandardPrice, trailerStandardPrice, truckStandardPrice, truckTrailerQuickWashPrice, trailerQuickWashPrice, truckQuickWashPrice;
        private int id;
        private bool hasId;

        public TruckWash(int id, string name, string address, string zip, string city, string phone, string email,
            string openingHours, string truckStandardPrice, string trailerStandardPrice, string truckTrailerStandardPrice,
           string truckQuickWashPrice, string trailerQuickWashPrice, string truckTrailerQuickWashPrice,
           string remarks, string website, string latitude, string longitude)
        {
            this.id = id;
            this.name = name;
            this.address = address;
            this.zip = zip;
            this.city = city;
            this.phone = phone;
            this.email = email;
            this.openingHours = openingHours;
            this.truckStandardPrice = truckStandardPrice;
            this.trailerStandardPrice = trailerStandardPrice;
            this.truckTrailerStandardPrice = truckTrailerStandardPrice;
            this.truckQuickWashPrice = truckQuickWashPrice;
            this.trailerQuickWashPrice = trailerQuickWashPrice;
            this.truckTrailerQuickWashPrice = truckTrailerQuickWashPrice;
            this.remarks = remarks;
            this.website = website;
            this.latitude = latitude;
            this.longitude = longitude;
            this.hasId = true;
        }
        public TruckWash(string name, string address, string zip, string city, string phone, string email, string openingHours,
             string truckStandardPrice, string trailerStandardPrice, string truckTrailerStandardPrice,
           string truckQuickWashPrice, string trailerQuickWashPrice, string truckTrailerQuickWashPrice,
            string remarks, string website, string latitude, string longitude)
        {
            id = 0;
            this.name = name;
            this.address = address;
            this.zip = zip;
            this.city = city;
            this.phone = phone;
            this.email = email;
            this.openingHours = openingHours;
            this.truckStandardPrice = truckStandardPrice;
            this.trailerStandardPrice = trailerStandardPrice;
            this.truckTrailerStandardPrice = truckTrailerStandardPrice;
            this.truckQuickWashPrice = truckQuickWashPrice;
            this.trailerQuickWashPrice = trailerQuickWashPrice;
            this.truckTrailerQuickWashPrice = truckTrailerQuickWashPrice;
            this.remarks = remarks;
            this.website = website;
            this.latitude = latitude;
            this.longitude = longitude;

            this.hasId = false;
        }

        public string Name { get => name; set => name = value; }
        public string Address { get => address; set => address = value; }
        public string Zip { get => zip; set => zip = value; }
        public string City { get => city; set => city = value; }
        public string Phone { get => phone; set => phone = value; }
        public string Email { get => email; set => email = value; }
        public string OpeningHours { get => openingHours; set => openingHours = value; }
        public string TruckStandardPrice { get => truckStandardPrice; set => truckStandardPrice = value; }
        public string TrailerStandardPrice { get => trailerStandardPrice; set => trailerStandardPrice = value; }
        public string TruckTrailerStandardPrice { get => truckTrailerStandardPrice; set => truckTrailerStandardPrice = value; }
        public string TruckQuickWashPrice { get => truckQuickWashPrice; set => truckQuickWashPrice = value; }
        public string TrailerQuickWashPrice { get => trailerQuickWashPrice; set => trailerQuickWashPrice = value; }
        public string TruckTrailerQuickWashPrice { get => truckTrailerQuickWashPrice; set => truckTrailerQuickWashPrice = value; }
        public string Remarks { get => remarks; set => remarks = value; }
        public string Website { get => website; set => website = value; }
        public string Latitude { get => latitude; set => latitude = value; }
        public string Longitude { get => longitude; set => longitude = value; }
        public int Id { get => id; set => id = value; }
        public bool HasId { get => hasId; set => hasId = value; }

        public override string ToString()
        {
            return name
               + "\n" + address
               + "\n" + zip
               + "\n" + city
               + "\n" + phone
               + "\n" + email
               + "\n" + openingHours
               + "\n" + truckStandardPrice
               + "\n" + trailerStandardPrice
               + "\n" + TruckTrailerStandardPrice
               + "\n" + truckQuickWashPrice
               + "\n" + trailerQuickWashPrice
               + "\n" + truckTrailerQuickWashPrice
               + "\n" + remarks
               + "\n" + website
               + "\n" + latitude
               + "\n" + longitude;
        }

        /*   public string[] TruckWashToArray()
           {
               string[] arr = { id.ToString(), name, address, zip, city,phone,email,openingHours,
                    truckStandardPrice.ToString(),trailerStandardPrice.ToString(),truckTrailerStandardPrice.ToString(),
                   truckQuickWashPrice.ToString(),trailerQuickWashPrice.ToString(), truckTrailerQuickWashPrice.ToString(),
                   remarks,website,longitude,latitude};
               return arr;
           }*/
    }
}
