﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Haulier
    {
        private int id;
        private string name, email, truckNr;
        private LoginUser loginUser;
        private Broker broker;
        private bool hasId;

        public Haulier(string name, string email, string truckNr, LoginUser loginUser)
        {
            this.name = name;
            this.email = email;
            this.truckNr = truckNr;
            this.loginUser = loginUser;
            hasId = false;
            broker = null;
        }

        public Haulier(int id, string name, string email, string truckNr, LoginUser loginUser)
        {
            this.id = id;
            this.name = name;
            this.email = email;
            this.truckNr = truckNr;
            this.loginUser = loginUser;
            hasId = true;
        }

        public Haulier(string name, string email, string truckNr, LoginUser loginUser, Broker broker)
        {
            this.name = name;
            this.email = email;
            this.truckNr = truckNr;
            this.loginUser = loginUser;
            this.broker = broker;
            hasId = false;
        }

        public Haulier(int id, string name, string email, string truckNr, LoginUser loginUser, Broker broker)
        {
            this.id = id;
            this.name = name;
            this.email = email;
            this.truckNr = truckNr;
            this.loginUser = loginUser;
            this.broker = broker;
            hasId = true;
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Email { get => email; set => email = value; }
        public string TruckNr { get => truckNr; set => truckNr = value; }
        public LoginUser LoginUser { get => loginUser; set => loginUser = value; }
        public Broker Broker { get => broker; set => broker = value; }
        public bool HasId { get => hasId; set => hasId = value; }
        public bool HasBroker()
        {
            return broker != null;
        }

        public override string ToString()
        {
            return name +
                 "\n" + email +
                 "\n" + truckNr +
                 "\n" + broker +
                 "\n" + loginUser;
        }
    }
}
