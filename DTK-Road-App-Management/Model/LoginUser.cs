﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace Model
{
    public enum Rights
    {
        [Description("Administrator")]
        ADMIN,
        [Description("User")]
        USER
    }
    public class LoginUser
    {
        private int id;
        private string username, password;
        private Rights rights;
        private bool hasId;


        public LoginUser(string username, string password, Rights rights)
        {
            this.username = username;
            this.password = password;
            this.rights = rights;
            hasId = false;
        }

        public LoginUser(int id, string username, string password, Rights rights)
        {
            this.id = id;
            this.username = username;
            this.password = password;
            this.rights = rights;
            hasId = true;
        }

        public int Id { get => id; set => id = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public Rights Rights { get => rights; set => rights = value; }
        public bool HasId { get => hasId; set => hasId = value; }

        public bool IsAdminsitrator()
        {
            return rights.Equals(Rights.ADMIN);
        }
        public bool IsUser()
        {
            return rights.Equals(Rights.USER);
        }

        public override string ToString()
        {
            return username +
                 "\n" + password +
                  "\n" + Utilities.GetEnumDescription(rights);
        }
    }
}
