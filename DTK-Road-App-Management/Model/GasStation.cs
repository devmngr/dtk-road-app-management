﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class GasStation
    {
        private string type, name, address, zip, city, longitude, latitude, adblue;
        private int id;
        private bool hasId;


        public GasStation(int id, string type, string name, string address, string zip, string city, string longitude, string latitude, string adblue)
        {
            this.id = id;
            this.type = type;
            this.name = name;
            this.address = address;
            this.zip = zip;
            this.city = city;
            this.longitude = longitude;
            this.latitude = latitude;
            this.adblue = adblue;
            hasId = true;
        }

        public GasStation(string type, string name, string address, string zip, string city, string longitude, string latitude, string adblue)
        {
            this.type = type;
            this.name = name;
            this.address = address;
            this.zip = zip;
            this.city = city;
            this.longitude = longitude;
            this.latitude = latitude;
            this.adblue = adblue;
            hasId = false;
        }
        public string Type { get => type; set => type = value; }
        public string Name { get => name; set => name = value; }
        public string Address { get => address; set => address = value; }
        public string Zip { get => zip; set => zip = value; }
        public string City { get => city; set => city = value; }
        public string Longitude { get => longitude; set => longitude = value; }
        public string Latitude { get => latitude; set => latitude = value; }
        public string AdBlue { get => adblue; set => adblue = value; }
        public int Id { get => id; set => id = value; }
        public bool HasId { get => hasId; set => hasId = value; }

        public override string ToString()
        {

            return id + "\t" + type + "\t" + name + "\t" + address + "\t" + zip + "\t" + city + "\t" + longitude + "\t" + latitude + "\t" + adblue;
        }

        public string[] GasStationToArray()
        {
            string[] arr = { id.ToString(), type, name, address, zip, city, longitude, latitude, adblue };
            return arr;
        }
    }
}
