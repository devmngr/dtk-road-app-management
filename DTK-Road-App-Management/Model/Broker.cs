﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Broker
    {
        private int id;
        private string name, email;
        private LoginUser loginUser;
        private bool hasId;

        public Broker(string name, string email, LoginUser loginUser)
        {
            this.name = name;
            this.email = email;
            this.loginUser = loginUser;
            hasId = false;
        }

        public Broker(int id, string name, string email, LoginUser loginUser)
        {
            this.id = id;
            this.name = name;
            this.email = email;
            this.loginUser = loginUser;
            hasId = true;
        }
        public Broker(int id, string name, string email)
        {
            this.id = id;
            this.name = name;
            this.email = email;
            this.loginUser = null;
            hasId = true;
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Email { get => email; set => email = value; }
        public LoginUser LoginUser { get => loginUser; set => loginUser = value; }
        public bool HasId { get => hasId; set => hasId = value; }

        public override string ToString()
        {
            return name +
                 "\n" + email +
                  "\n" + loginUser;
        }
    }
}
